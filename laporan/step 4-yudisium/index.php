
<?php
// Kode by: ok maryanto,S.Kom
include_once("../conn.php");
if (isset($_POST['submit'])) {
    $SEMESTER = $_POST['semester'];
    $KELAS = $_POST['kelas'];
    $JUR = substr($KELAS, 2, 2);
    $MASUK = substr($KELAS, 5, 2);
    $pecahkelas = explode("/", $KELAS);
    $kelas1 = strtoupper($pecahkelas[0]); // buat huruf besar semua 
    $kelas2 = strtoupper($pecahkelas[1]);
    $kdkonsen = $JUR;
    if ($kdkonsen == "OF") {
        $JUR = "61401";
        $KONSEN = "MANAJEMEN ADMINISTRASI OBAT DAN FARMASI";
        $KODEX = "33";
    } elseif ($kdkonsen == "RS") {
        $JUR = "61401";
        $KODEX = "03";
        $KONSEN = "MANAJEMEN ADMINISTRASI RUMAH SAKIT";
    } elseif ($kdkonsen == "TU") {
        $JUR = "61401";
        $KODEX = "13";
        $KONSEN = "MANAJEMEN ADMINISTRASI TRANSPORTASI UDARA";
    }
    $PRODI = "MANAJEMEN ADMINISTRASI";
    $NAMAP = "MANAJEMEN ADMINISTRASI";

    $TAHUN = "20$MASUK";
    $ASALXX = "20$MASUK";
    $ASALXX2 = "1";
    $CEKXX = substr($SEMESTER, 0, 4);
    $CEKXX2 = substr($SEMESTER, 4, 1);
    $CEKXXX = $CEKXX + 1;


    if ($ASALXX2 == $CEKXX2) {
        $P = (($CEKXX - $ASALXX) * 2) + 1;
    } else {
        $P = (($CEKXX - $ASALXX) * 2) + 2;
    }
    if (($P % 2) == 1) {
        $SS = "GANJIL";
    } else {
        $SS = "GENAP";
    }
    if ($P == 1) {
        $R = "I";
    } elseif ($P == 2) {
        $R = "II";
    } elseif ($P == 3) {
        $R = "III";
    } elseif ($P == 4) {
        $R = "IV";
    } elseif ($P == 5) {
        $R = "V";
    } elseif ($P == 6) {
        $R = "VI";
    } elseif ($P == 7) {
        $R = "VII";
    } elseif ($P == 8) {
        $R = "VIII";
    } elseif ($P == 9) {
        $R = "IX";
    } elseif ($P == 10) {
        $R = "X";
    } elseif ($P == 11) {
        $R = "XI";
    } elseif ($P == 12) {
        $R = "XII";
    } elseif ($P == 13) {
        $R = "XIII";
    } elseif ($P == 14) {
        $R = "XIV";
    } elseif ($P == 15) {
        $R = "XV";
    } else {
        $R = "??";
    }

    $SEMESTERAN = substr($SEMESTER, 4, 2);

    $SEMESTERAN = "0$SEMESTERAN";
    $KODETH = "$KODEX$MASUK";

    $qall = "SELECT * FROM msmhs m,kelasparalel_mhs k where m.TAHUNMSMHS='$TAHUN' and k.nimhs=m.NIMHSMSMHS and k.nmkelas='$KELAS' and m.KDPSTMSMHS='$JUR' order by m.NIMHSMSMHS ASC";

    $hasilall = mysql_query($qall);
    ?>
<html>
    <head>
        <title><? print("$kelas1");?>-<? print("$kelas2");?>-<? print("$R");?>-<? print("$SEMESTER");?></title>
        <style>


            body {
                background-color: #ffffff; /* background color */
                color: inherit; /* text color */
                font-family: Arial Narrow; /* font name */
                font-size: 14; /* font size */
                margin: 0px 0px 0px 0px; /* top right bottom left */
            }

            .phpreportmaker {
                color: inherit; /* text color */
                font-family: Verdana; /* font name */
                font-size: xx-small; /* font size */	
            }

            /* main table */
            .ewTable {
                width: inherit; /* table width */	
                color: inherit; /* text color */
                font-family: Arial Narrow; /* font name */
                font-size: 10; /* font size */

                border-collapse: collapse;
            }

            /* main table data cells */
            .ewTable td {
                padding: 3px; /* cell padding */

                border-color: #000000;  /* table background color */
            }

            /* main table header cells */
            .ewTableHeader {
                background-color: #6699CC; /* header color */
                color: #FFFFFF; /* header font color */	
                vertical-align: top;	
            }

            .ewTableHeader a:link {	
                color: #FFFFFF; /* header font color */	
            }

            .ewTableHeader a:visited {	
                color: #FFFFFF; /* header font color */	
            }

            /* main table row color */
            .ewTableRow {
                background-color: #FFFFFF;  /* alt row color 1 */
            }

            /* main table alternate row color */
            .ewTableAltRow {
                background-color: #F5F5F5; /* alt row color 2 */	
            }


            /* group 1 */
            .ewRptGrpHeader1 {
                background-color: #CCFFFF;
                font-weight: bold;		
            }

            .ewRptGrpField1 {
                background-color: #CCFFFF;
            }

            .ewRptGrpSummary1 {
                background-color: #BBEEEE;	
            }

            /* group 2 */
            .ewRptGrpHeader2 {
                background-color: #CCFFCC;
                font-weight: bold;
            }

            .ewRptGrpField2 {
                background-color: #CCFFCC;
            }

            .ewRptGrpSummary2 {
                background-color: #BBEEBB;	
            } 

            /* group 3 */
            .ewRptGrpHeader3 {
                background-color: #99FFCC;
                font-weight: bold;	
            }

            .ewRptGrpField3 {
                background-color: #99FFCC;
            }

            .ewRptGrpSummary3 {
                background-color: #88EEBB;	
            }

            /* group 4 */
            .ewRptGrpHeader4 {
                background-color: #99FF99;
                font-weight: bold;	
            }

            .ewRptGrpField4 {
                background-color: #99FF99;
            }

            .ewRptGrpSummary4 {
                background-color: #88EE88;	
            }

            .ewRptGrpAggregate {
                font-weight: bold;
            }

            .ewRptPageSummary {
                background-color: #FFFFCC; /* page total background color */	
            }

            .ewRptGrandSummary {
                background-color: #FFFF66; /* grand total background color */	
            }

            /* classes for crosstab report only */

            .ewRptColHeader {
                background-color: #CCFF66; /* column background color */
                font-weight: bold;
            }

            .ewRptColField {
                background-color: #CCFF66; /* column background color */
            }



        </style>
    </head>
    <body>
        <B>REKAP NILAI SEMESTER <? print("$R");?> ( <? print("$SS");?> ) TAHUN AJARAN <? print("$CEKXX");?>/<? print("$CEKXXX");?></B><BR>
        <B>AMA YOGYAKARTA PRODI : <? print("$NAMAP");?></B><BR><BR>
        <B>KELAS : <? print("$kelas1");?> / <? print("$kelas2");?> / <? print("$R");?></B><BR>
        <?
        $perintah=mysql_query("select * from kelasparalel k,msdos t where k.namakelas='$KELAS' and t.NODOSMSDOS=k.nodos"); 
        $datanya=mysql_fetch_array($perintah);
        $namados=$datanya['NMDOSMSDOS'];
        ?>
        <B>DOSEN WALI : <? print("$namados");?></B><BR>
        <br>
        <div id="report_summary">
            <table id="ewReport" class="ewTable" border=1>
                <tbody><tr class="ewTableAltRow">

                        <td class="ewRptDtlField" align="center" valign="center">No. </td>
                        <td class="ewRptDtlField" align="center" valign="center">NIM</td>
                        <td class="ewRptDtlField" align="center" valign="center">Nama</td>

                        <?
                        if($SEMESTERAN=="01")
                        {
                        //$qmapel = "SELECT distinct(mk.KDKMKTBKMK),mk.NAKMKTBKMK,mk.SKSMKTBKMK,mk.SEMESTBKMK FROM tbkmk mk,trnlm n,kelasparalel_mhs k where mk.THSMSTBKMK='$SEMESTER' and (mk.SEMESTBKMK='01' or mk.SEMESTBKMK='02' or mk.SEMESTBKMK='03' or mk.SEMESTBKMK='04' or mk.SEMESTBKMK='05' or mk.SEMESTBKMK='06' or mk.SEMESTBKMK='07' or mk.SEMESTBKMK='08') and mk.KDPSTTBKMK='$JUR' and n.KDKMKTRNLM=mk.KDKMKTBKMK AND n.THSMSTRNLM='$SEMESTER' and LEFT(n.NIMHSTRNLM,4) = '$KODETH' and k.nimhs=n.NIMHSTRNLM and k.nmkelas='$KELAS' and (mk.kdkonsen='u' or mk.kdkonsen='$kdkonsen')  order by mk.SEMESTBKMK,mk.KDKMKTBKMK asc";
                        $qmapel = "SELECT distinct(mk.KDKMKTBKMK),mk.NAKMKTBKMK,mk.SKSMKTBKMK,mk.SEMESTBKMK FROM tbkmk mk,trnlm n,kelasparalel_mhs k where mk.THSMSTBKMK='$SEMESTER' and (mk.SEMESTBKMK='01' or mk.SEMESTBKMK='02' or mk.SEMESTBKMK='03' or mk.SEMESTBKMK='04' or mk.SEMESTBKMK='05' or mk.SEMESTBKMK='06' or mk.SEMESTBKMK='07' or mk.SEMESTBKMK='08') and mk.KDPSTTBKMK='$JUR' and n.KDKMKTRNLM=mk.KDKMKTBKMK AND n.THSMSTRNLM='$SEMESTER'  and k.nimhs=n.NIMHSTRNLM and k.nmkelas='$KELAS' and (mk.kdkonsen='u' or mk.kdkonsen='$kdkonsen')  order by mk.SEMESTBKMK,mk.KDKMKTBKMK asc";
                        }else
                        {
                        $qmapel = "SELECT distinct(mk.KDKMKTBKMK),mk.NAKMKTBKMK,mk.SKSMKTBKMK,mk.SEMESTBKMK FROM tbkmk mk,trnlm n,kelasparalel_mhs k where mk.THSMSTBKMK='$SEMESTER' and (mk.SEMESTBKMK='01' or mk.SEMESTBKMK='02' or mk.SEMESTBKMK='03' or mk.SEMESTBKMK='04' or mk.SEMESTBKMK='05' or mk.SEMESTBKMK='06' or mk.SEMESTBKMK='07' or mk.SEMESTBKMK='08') and mk.KDPSTTBKMK='$JUR' and n.KDKMKTRNLM=mk.KDKMKTBKMK AND n.THSMSTRNLM='$SEMESTER' and k.nimhs=n.NIMHSTRNLM and k.nmkelas='$KELAS' and (mk.kdkonsen='u' or mk.kdkonsen='$kdkonsen')  order by mk.SEMESTBKMK,mk.KDKMKTBKMK asc";
                        //$qmapel = "SELECT distinct(mk.KDKMKTBKMK),mk.NAKMKTBKMK,mk.SKSMKTBKMK,mk.SEMESTBKMK FROM tbkmk mk,trnlm n,kelasparalel_mhs k where mk.THSMSTBKMK='$SEMESTER' and (mk.SEMESTBKMK='01' or mk.SEMESTBKMK='02' or mk.SEMESTBKMK='03' or mk.SEMESTBKMK='04' or mk.SEMESTBKMK='05' or mk.SEMESTBKMK='06' or mk.SEMESTBKMK='07' or mk.SEMESTBKMK='08') and mk.KDPSTTBKMK='$JUR' and n.KDKMKTRNLM=mk.KDKMKTBKMK AND n.THSMSTRNLM='$SEMESTER' and LEFT(n.NIMHSTRNLM,4) = '$KODETH' and k.nimhs=n.NIMHSTRNLM and k.nmkelas='$KELAS' and (mk.kdkonsen='u' or mk.kdkonsen='$kdkonsen')  order by mk.SEMESTBKMK,mk.KDKMKTBKMK asc";
                        }

                        $hasilmapel = mysql_query($qmapel);
                        $tr=0;
                        while($datamapel = mysql_fetch_array($hasilmapel))
                        {
                        $tr++;
                        $nm_mapel= $datamapel["NAKMKTBKMK"];
                        $kode_mapel= $datamapel["KDKMKTBKMK"];
                        $sks_mapel= $datamapel["SKSMKTBKMK"];
                        $sem_mapel= $datamapel["SEMESTBKMK"];
                        //$qall21 = "SELECT * FROM trnlm n where n.KDKMKTRNLM='$kode_mapel' AND LEFT(n.NIMHSTRNLM,4) = '$KODETH'";
                        $qall21 = "SELECT * FROM trnlm n where n.KDKMKTRNLM='$kode_mapel'";
                        $hasilall21 = mysql_query($qall21);
                        $xcv=0;
                        while($dataall21 = mysql_fetch_array($hasilall21)){
                        $xcv++;
                        }
                        if($xcv>0)
                        {
                        ?>
                        <td class="ewRptDtlField">
                            <? print("$nm_mapel");?>


                        </td>

                        <?
                        }
                        }
                        ?>
                        <td class="ewRptDtlField">TOTAL SKS</td>

                        <td class="ewRptDtlField">IPSEM</td>
                        <td class="ewRptDtlField">Predikat</td>
                    </tr>


                    <?
                    $noxcc=0;
                    while($dataall = mysql_fetch_array($hasilall))
                    {
                    $noxcc++;
                    $nim= $dataall["NIMHSMSMHS"];
                    $nama= $dataall["NMMHSMSMHS"];
                    $kelas= $dataall["nmkelas"];

                    ?>
                    <tr class="ewTableRow">
                        <td bgcolor="#ffffff" align="center"><? print("$noxcc");?></td>
                        <td bgcolor="#ffffff" align="center" valign="center"><? print("$nim");?></td>
                        <td bgcolor="#ffffff" >&nbsp;&nbsp;<? print("$nama");?></td>


                        <?

                        if($SEMESTERAN=="01")
                        {
                        //$qmapel2 = "SELECT distinct(mk.KDKMKTBKMK),mk.NAKMKTBKMK,mk.SKSMKTBKMK,mk.SEMESTBKMK FROM tbkmk mk,trnlm n,kelasparalel_mhs k where mk.THSMSTBKMK='$SEMESTER' and (mk.SEMESTBKMK='01' or mk.SEMESTBKMK='02' or mk.SEMESTBKMK='03' or mk.SEMESTBKMK='04' or mk.SEMESTBKMK='05' or mk.SEMESTBKMK='06' or mk.SEMESTBKMK='07' or mk.SEMESTBKMK='08') and mk.KDPSTTBKMK='$JUR' and n.KDKMKTRNLM=mk.KDKMKTBKMK AND n.THSMSTRNLM='$SEMESTER' and LEFT(n.NIMHSTRNLM,4) = '$KODETH' and k.nimhs=n.NIMHSTRNLM and k.nmkelas='$KELAS' and (mk.kdkonsen='u' or mk.kdkonsen='$kdkonsen')   order by mk.SEMESTBKMK,mk.KDKMKTBKMK asc";
                        $qmapel2 = "SELECT distinct(mk.KDKMKTBKMK),mk.NAKMKTBKMK,mk.SKSMKTBKMK,mk.SEMESTBKMK FROM tbkmk mk,trnlm n,kelasparalel_mhs k where mk.THSMSTBKMK='$SEMESTER' and (mk.SEMESTBKMK='01' or mk.SEMESTBKMK='02' or mk.SEMESTBKMK='03' or mk.SEMESTBKMK='04' or mk.SEMESTBKMK='05' or mk.SEMESTBKMK='06' or mk.SEMESTBKMK='07' or mk.SEMESTBKMK='08') and mk.KDPSTTBKMK='$JUR' and n.KDKMKTRNLM=mk.KDKMKTBKMK AND n.THSMSTRNLM='$SEMESTER' and k.nimhs=n.NIMHSTRNLM and k.nmkelas='$KELAS' and (mk.kdkonsen='u' or mk.kdkonsen='$kdkonsen')   order by mk.SEMESTBKMK,mk.KDKMKTBKMK asc";
                        }else
                        {
                        //$qmapel2 = "SELECT distinct(mk.KDKMKTBKMK),mk.NAKMKTBKMK,mk.SKSMKTBKMK,mk.SEMESTBKMK FROM tbkmk mk,trnlm n,kelasparalel_mhs k where mk.THSMSTBKMK='$SEMESTER' and (mk.SEMESTBKMK='01' or mk.SEMESTBKMK='02' or mk.SEMESTBKMK='03' or mk.SEMESTBKMK='04' or mk.SEMESTBKMK='05' or mk.SEMESTBKMK='06' or mk.SEMESTBKMK='07' or mk.SEMESTBKMK='08') and mk.KDPSTTBKMK='$JUR' and n.KDKMKTRNLM=mk.KDKMKTBKMK AND n.THSMSTRNLM='$SEMESTER' and LEFT(n.NIMHSTRNLM,4) = '$KODETH' and k.nimhs=n.NIMHSTRNLM and k.nmkelas='$KELAS' and (mk.kdkonsen='u' or mk.kdkonsen='$kdkonsen')   order by mk.SEMESTBKMK,mk.KDKMKTBKMK asc";
                        $qmapel2 = "SELECT distinct(mk.KDKMKTBKMK),mk.NAKMKTBKMK,mk.SKSMKTBKMK,mk.SEMESTBKMK FROM tbkmk mk,trnlm n,kelasparalel_mhs k where mk.THSMSTBKMK='$SEMESTER' and (mk.SEMESTBKMK='01' or mk.SEMESTBKMK='02' or mk.SEMESTBKMK='03' or mk.SEMESTBKMK='04' or mk.SEMESTBKMK='05' or mk.SEMESTBKMK='06' or mk.SEMESTBKMK='07' or mk.SEMESTBKMK='08') and mk.KDPSTTBKMK='$JUR' and n.KDKMKTRNLM=mk.KDKMKTBKMK AND n.THSMSTRNLM='$SEMESTER' and k.nimhs=n.NIMHSTRNLM and k.nmkelas='$KELAS' and (mk.kdkonsen='u' or mk.kdkonsen='$kdkonsen')   order by mk.SEMESTBKMK,mk.KDKMKTBKMK asc";
                        }
                        $hasilmapel2 = mysql_query($qmapel2);
                        while($datamapel2 = mysql_fetch_array($hasilmapel2))
                        {
                        $kode_mapel= $datamapel2["KDKMKTBKMK"];
                        $sks_mapel= $datamapel2["SKSMKTBKMK"];
                        //$qall211 = "SELECT * FROM trnlm n where n.KDKMKTRNLM='$kode_mapel' AND LEFT(n.NIMHSTRNLM,4) = '$KODETH'";
                        $qall211 = "SELECT * FROM trnlm n where n.KDKMKTRNLM='$kode_mapel'";
                        $hasilall211 = mysql_query($qall211);
                        $xcvz=0;
                        while($dataall211 = mysql_fetch_array($hasilall211)){
                        $xcvz++;
                        }
                        if($xcvz>0)
                        {

                        $qall2 = "SELECT * FROM trnlm n where n.KDKMKTRNLM='$kode_mapel' and n.NIMHSTRNLM='$nim'  AND n.THSMSTRNLM='$SEMESTER'";
                        $hasilall2 = mysql_query($qall2);
                        $xc=0;
                        while($dataall2 = mysql_fetch_array($hasilall2))
                        {
                        $xc++;
                        $nilai= $dataall2["NLAKHTRNLM"];
                        $bobot= $dataall2["BOBOTTRNLM"];

                        $sks_mapelx=$sks_mapelx+$sks_mapel;
                        if($sks_mapelx>24)
                        {
                        ?>
                        <td bgcolor="#ffffff" align="center"><b>Redudansi Data</b></td>
                        <?
                        }else
                        {													
                        if($nilai=="")
                        {



                        ?>
                        <td bgcolor="#ffffff" align="center"><b>0</b></td>

                        <?

                        }else
                        {
                        $bobotc=$sks_mapel*$bobot;
                        $bobotx=$bobotx+$bobotc;
                        ?>
                        <td bgcolor="#ffffff" align="center"><b><? print("$nilai");?></b></td>
                        <?
                        }

                        }
                        }
                        ?>

                        <?
                        if($xc==0)
                        {
                        ?>
                        <td bgcolor="#ffffff" align="center"></td>

                        <?
                        }

                        }


                        }
                        ?>						
                        <?
                        $totsks=$sks_mapelx;
                        $totbobot=$bobotx;
                        $ipsemx=$ipsemx;
                        $namax=$namax;
                        if($totsks>24)
                        {
                        ?>
                        <td bgcolor="#ffffff" align="center">Redudansi Data</td>
                        <?
                        }else
                        {
                        if($totsks<=0 and $totbobot<=0)
                        {
                        $ipsem=0;
                        }else
                        {
                        $ipsem=$totbobot/$totsks;
                        $ipsem=number_format($ipsem,2); 

                        }
                        if($ipsem>$ipsemx)
                        {
                        $namax=$nama;
                        $nimx=$nim;
                        $ipsemx=$ipsem;
                        }else
                        {
                        $namax=$namax;
                        $nimx=$nimx;
                        $ipsemx=$ipsemx;
                        }

                        //$updatenilai = "UPDATE trakm SET NLIPSTRAKM=$ipsem,SKSEMTRAKM=$sks_mapelx,bobottotal=$totbobot where NIMHSTRAKM='$nim' and THSMSTRAKM='$SEMESTER'";
                        //$hasilall211 = mysql_query($updatenilai);
                        ?>
                        <td bgcolor="#ffffff" align="center"><? print("$sks_mapelx");?></td>					  

                        <td bgcolor="#ffffff" align="center"><b><? print("$ipsem");?></b></td>
                        <?
                        if($ipsem>=3.51)
                        {
                        $predikat="Cumlaude";
                        }elseif($ipsem>=2.76)
                        {
                        $predikat="Sangat Memuaskan";
                        }elseif($ipsem>=2)
                        {
                        $predikat="Memuaskan";
                        }elseif($ipsem>0)
                        {
                        $predikat="Kurang Memuaskan";
                        }elseif($ipsem<=0)
                        {
                        $predikat="--";
                        }
                        ?>
                        <td bgcolor="#ffffff">&nbsp;<? print("$predikat");?></td>
                        <?
                        }

                        ?>						


                    </tr>

                    <?
                    $sks_mapelx=0;
                    $bobotx=0;
                    }
                    ?>

                </tbody></table><br>IPSEM Tertinggi : <b><? print("$ipsemx");?></b><br>
            Informasi :		<br>

            1. Mahasiswa yang namanya tidak tercantum dalam Yudisium, harap menghubungi bagian AKADEMIK<br>																	
            2. Apabila terdapat kesalahan atau kekeliuran dalam nilai, <br>
            &nbsp;&nbsp;&nbsp;&nbsp;silahkan melakukan KROSCEK NILAI ke bagian AKADEMIK paling lambat 1 bulan setelah UAS<br>
            3. Apabila terdapat perbedaan dalam nilai rekap dengan KHS, <BR>
            &nbsp;&nbsp;&nbsp;&nbsp;maka data yang benar adalah nilai KHS, atau hubungi akademik																	<br>
            <br>

            <?
            $engDate=date("l F d, Y H:i:s A");
            //echo "English Date : ". $engDate ."<p>";

            switch (date("w")) {
            case "0" : $hari="Minggu";break;
            case "1" : $hari="Senin";break;
            case "2" : $hari="Selasa";break;
            case "3" : $hari="Rabu";break;
            case "4" : $hari="Kamis";break;
            case "5" : $hari="Jumat";break;
            case "6" : $hari="Sabtu";break;
            } switch (date("m")) {
            case "1" : $bulan="Januari";break;
            case "2" : $bulan="Februari";break;
            case "3" : $bulan="Maret";break;
            case "4" : $bulan="April";break;
            case "5" : $bulan="Mei";break;
            case "6" : $bulan="Juni";break;
            case "7" : $bulan="Juli";break;
            case "8" : $bulan="Agustus";break;
            case "9" : $bulan="September";break;
            case "10" : $bulan="Oktober";break;
            case "11" : $bulan="November";break;
            case "12" : $bulan="Desember";break;
            }
            $indDate="$hari, ". date("d") ." $bulan". date(" Y");

            ?>
            Dicetak : <? echo "". $indDate ." Bagian Akademik";?><br>																		
            Generated : Software SIAKAD v.1 beta (c)Web IT Solution<br><br>


            <?

            }else
            {
            ?>
            <h2>Preview Yudisium AMA YOGYAKARTA<br>Code:Web IT Solution</h2>
            <hr />
            <br>

            <form action="" method="post" name="form">
                <table width="100%" class="biasa">
                    <tr>
                        <td><table>
                                <tr>
                                    <td>Thn. Akdmk/Semester</td>
                                    <td>:</td>
                                    <td><label>
                                            <select name="semester">

                                                <?
                                                $qpeg2 = "SELECT distinct(THSMSTBKMK) FROM tbkmk order by THSMSTBKMK DESC";
                                                $datapeg2 = mysql_query($qpeg2);
                                                ?>
                                                <?
                                                while($dataall2 = mysql_fetch_array($datapeg2))
                                                {
                                                $NM_SEM_O = $dataall2["THSMSTBKMK"];


                                                print("<option value=\"$NM_SEM_O\">$NM_SEM_O</option>");
                                                }
                                                ?>
                                            </select>
                                        </label></td>
                                </tr>

                                <tr>
                                    <td>Kelas</td>
                                    <td>:</td>
                                    <td><label>
                                            <select name="kelas">

                                                <?
                                                $qpeg2 = "SELECT namakelas FROM kelasparalel  order by namakelas ASC";
                                                $datapeg2 = mysql_query($qpeg2);
                                                ?>
                                                <?
                                                while($dataall2 = mysql_fetch_array($datapeg2))
                                                {
                                                $NM_SEMA_O = $dataall2["namakelas"];


                                                print("<option value=\"$NM_SEMA_O\">$NM_SEMA_O</option>");
                                                }
                                                ?>
                                            </select>
                                        </label></td>
                                </tr>
                                <tr>
                                    <td align="right">
                                    </td>
                                    <td>&nbsp;</td>
                                    <td>
                                        <input type="submit" name="submit" value="Lihat">						  </td>
                                </tr></tbody>
                            </table>
                            </form>
                            <br><br>


                            <?
                            //include("komentar.php");
                            }
                            ?>
                            </div>
                            </body>
                            </html>
