<!--Progress Bar script- by Todd King (tking@igpp.ucla.edu)-->
function ProgressCreate(end) {
	// Initialize state variables
	var duration=3 // Specify duration of progress bar in seconds
	var _progressWidth = 50;	// Display width of progress bar.
	var _progressBar = "***********************************************************************************************************************"
	var _progressEnd = 5;
	var _progressAt = 0;
	_progressEnd = end;
	_progressAt = 0;
	
	// Create layer for progress dialog
	document.write("<span id=\"progress\" class=\"hide\">");
	document.write("<FORM name=dialog id=dialog>");
	document.write("<TABLE border=2  bgcolor=\"#FFFFCC\">");
	document.write("<TR><TD ALIGN=\"center\">");
	document.write("Progress<BR>");
	document.write("<input type=text name=\"bar\" id=\"bar\" size=\"" + _progressWidth/2 + "\"");
	if(document.all||document.getElementById) 	// Microsoft, NS6
		document.write(" bar.style=\"color:navy;\">");
	else	// Netscape
		document.write(">");
	document.write("</TD></TR>");
	document.write("</TABLE>");
	document.write("</FORM>");
	document.write("</span>");

	// Move layer to center of window to show
	if (document.all) {	// Internet Explorer
		progress.className = 'show';
		progress.style.left = (document.body.clientWidth/2) - (progress.offsetWidth/2);
		progress.style.top = document.body.scrollTop+(document.body.clientHeight/2) - (progress.offsetHeight/2);
	} else if (document.layers) {	// Netscape
		document.progress.visibility = true;
		document.progress.left = (window.innerWidth/2) - 100+"px";
		document.progress.top = pageYOffset+(window.innerHeight/2) - 40+"px";
	} else if (document.getElementById) {	// Netscape 6+
		document.getElementById("progress").className = 'show';
		document.getElementById("progress").style.left = (window.innerWidth/2)- 100+"px";
		document.getElementById("progress").style.top = pageYOffset+(window.innerHeight/2) - 40+"px";
	}

	ProgressUpdate();	// Initialize bar
}

// Hide the progress layer
function ProgressDestroy() {
	// Move off screen to hide
	if (document.all) {	// Internet Explorer
		progress.className = 'hide';
	} else if (document.layers) {	// Netscape
		document.progress.visibility = false;
	} else if (document.getElementById) {	// Netscape 6+
		document.getElementById("progress").className = 'hide';
	}
}

// Increment the progress dialog one step
function ProgressStepIt() {
	_progressAt++;
	if(_progressAt > _progressEnd) _progressAt = _progressAt % _progressEnd;
	ProgressUpdate();
}

// Update the progress dialog with the current state
function ProgressUpdate() {
	var n = (_progressWidth / _progressEnd) * _progressAt;
	if (document.all) {	// Internet Explorer
		var bar = dialog.bar;
 	} else if (document.layers) {	// Netscape
		var bar = document.layers["progress"].document.forms["dialog"].bar;
		n = n * 0.55;	// characters are larger
	} else if (document.getElementById){
                var bar=document.getElementById("bar")
        }
	var temp = _progressBar.substring(0, n);
	bar.value = temp;
}

// Demonstrate a use of the progress dialog.
function Demo() {
	ProgressCreate(10);
	window.setTimeout("Click()", 100);
}

function Click() {
	if(_progressAt >= _progressEnd) {
		ProgressDestroy();
		return;
	}
	ProgressStepIt();
	window.setTimeout("Click()", (duration-1)*200/10);
}

function progressbar(jsStr) { //v2.0
  return eval(jsStr)
}

ProgressDestroy();	// Hides
<!-- end fungsi -->

<!--fungsi untuk validasi form isian-->
function validasi(which)
{		
	var pass=true
	if (document.images){
		for (i=0;i<which.length;i++){
			var tempobj=which.elements[i]
			if (tempobj.name.substring(0,4)=="txt_"){
				if (((tempobj.type=="text"||tempobj.type=="password"||tempobj.type=="textarea")&&tempobj.value=='')||(tempobj.type.toString().charAt(0)=="s"&&tempobj.selectedIndex==-1)){
					pass = false;
					break;
				}
			}
		}
	}
	
	if (!pass){
		createShadow();
		alert("Mohon lengkapi kolom isian yang masih kosong !! !");
		clearShadow();
		return false;
	} else {
		progressbar('Demo()');
		return true;
	}
}
<!-- end fungsi -->

<!-- fungsi pesan hapus -->
function pesan_hapus()
{	
	if (!confirm('Yakin ingin menghapus data ini?')) {
		history.back;
	}
}
<!-- end fungsi -->

//fungsi untuk menampilkan pop up gambar
var popbackground="lightskyblue" //specify backcolor or background image for pop window
var windowtitle="Image Window"  //pop window title

function detectexist(obj){
	return (typeof obj !="undefined")
}
	
function jkpopimage(imgpath, popwidth, popheight, textdescription){
	
	function getpos(){
		leftpos=(detectexist(window.screenLeft))? screenLeft+document.body.clientWidth/2-popwidth/2 : detectexist(window.screenX)? screenX+innerWidth/2-popwidth/2 : 0
		toppos=(detectexist(window.screenTop))? screenTop+document.body.clientHeight/2-popheight/2 : detectexist(window.screenY)? screenY+innerHeight/2-popheight/2 : 0
		if (window.opera){
		leftpos-=screenLeft
		toppos-=screenTop
	}
}
	
getpos()
	var winattributes='width='+popwidth+',height='+popheight+',resizable=yes,left='+leftpos+',top='+toppos
	var bodyattribute=(popbackground.indexOf(".")!=-1)? 'background="'+popbackground+'"' : 'bgcolor="'+popbackground+'"'
	if (typeof jkpopwin=="undefined" || jkpopwin.closed)
	jkpopwin=window.open("","",winattributes)
	else{
		//getpos() //uncomment these 2 lines if you wish subsequent popups to be centered too
		//jkpopwin.moveTo(leftpos, toppos)
		jkpopwin.resizeTo(popwidth, popheight+30)
	}
	jkpopwin.document.open()
	jkpopwin.document.write('<html><title>'+windowtitle+'</title><body '+bodyattribute+'><img src="'+imgpath+'" style="margin-bottom: 0.5em"><br />'+textdescription+'</body></html>')
	jkpopwin.document.close()
	jkpopwin.focus()
}
<!-- end fungsi -->

<!-- fungsi untuk grayscale -->
var imageDir = "" //Images Directory
var shadowOnload = 0 //Onload? Yes=1 No=0
//End Configuration

function opacity(id, opacStart, opacEnd, millisec) {
var speed = Math.round(millisec / 100);
var timer = 0;
if(opacStart > opacEnd) {
	for(i = opacStart; i >= opacEnd; i--) {
		setTimeout("changeOpac(" + i + ",'" + id + "')",(timer * speed));
		timer++;
	}
} else if(opacStart < opacEnd) {
for(i = opacStart; i <= opacEnd; i++) {
		setTimeout("changeOpac(" + i + ",'" + id + "')",(timer * speed));
		timer++;
	}
}
}
function changeOpac(opacity, id) {
var object = document.getElementById(id).style;
object.opacity = (opacity / 100);
object.MozOpacity = (opacity / 100);
object.KhtmlOpacity = (opacity / 100);
object.filter = "alpha(opacity=" + opacity + ")";
}
var eCreated = 0
var windowOpen=0
function createShadow(effect) {

if (windowOpen==1) {

alert("Shadow already open!")
return false
}
if (eCreated==0) {
var s=document.createElement('div');
eCreated==1
s.className="shadow";
s.setAttribute("id", "overlay", 0);
document.body.appendChild(s);
opacity('overlay',0,60,700);
document.getElementById("overlay").innerHTML='<div align="right"><a href="javascript:clearShadow();" class="close">Close</a></div>'
}
else {
document.getElementById("overlay").style.display = "block"
opacity('overlay',0,60,700);
}

windowOpen==1

}
function clearShadow() {

opacity('overlay',60,0,700)
setTimeout("document.getElementById('overlay').style.display='none'",700)
}

function initShadow() {

if (shadowOnload==1) {

createShadow();

}
}
setTimeout("initShadow()",300)

function pesanlogout() {
	createShadow();
	if (confirm("Yakin ingin log out ?")) {
		clearShadow();
		return;
	}
	else {
		clearShadow();
	}
}

function createDialog(e) {
createShadow()
var dialogText = document.getElementById(e).innerHTML
document.getElementById("overlay").innerHTML += '<div id="d" class="dialog">'+dialogText+'</div>'
setTimeout("document.getElementById(\"d\").style.display = \"block\";opacity('d',0,100,500)",1000)
}

/* fungsi tekan tombol 
function getKey(e){
if (e == null) { // ie
keycode = event.keyCode;
} else { // mozilla
keycode = e.which;
}
key = String.fromCharCode(keycode).toLowerCase();

if(key == 'x'){
clearShadow();
}
if (key == 's'){
createShadow();
}
}
window.onkeyup=getKey;
*/
<!-- end fungsi -->