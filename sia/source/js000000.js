var WebService=function() {
WebService.initializeBase(this);
this._timeout = 0;
this._userContext = null;
this._succeeded = null;
this._failed = null;
}
WebService.prototype={
_get_path:function() {
 var p = this.get_path();
 if (p) return p;
 else return WebService._staticInstance.get_path();},
Getregid:function(prefixText,count,succeededCallback, failedCallback, userContext) {
return this._invoke(this._get_path(), 'Getregid',false,{prefixText:prefixText,count:count},succeededCallback,failedCallback,userContext); },
doCycleDays:function(startdate,enddate,cycledays,succeededCallback, failedCallback, userContext) {
return this._invoke(this._get_path(), 'doCycleDays',false,{startdate:startdate,enddate:enddate,cycledays:cycledays},succeededCallback,failedCallback,userContext); },
doStartDate:function(startdate,cycledays,succeededCallback, failedCallback, userContext) {
return this._invoke(this._get_path(), 'doStartDate',false,{startdate:startdate,cycledays:cycledays},succeededCallback,failedCallback,userContext); },
GetTowns:function(prefixText,succeededCallback, failedCallback, userContext) {
return this._invoke(this._get_path(), 'GetTowns',false,{prefixText:prefixText},succeededCallback,failedCallback,userContext); },
GetDistricts:function(prefixText,succeededCallback, failedCallback, userContext) {
return this._invoke(this._get_path(), 'GetDistricts',false,{prefixText:prefixText},succeededCallback,failedCallback,userContext); },
GetCounty:function(prefixText,succeededCallback, failedCallback, userContext) {
return this._invoke(this._get_path(), 'GetCounty',false,{prefixText:prefixText},succeededCallback,failedCallback,userContext); },
GetIM:function(succeededCallback, failedCallback, userContext) {
return this._invoke(this._get_path(), 'GetIM',false,{},succeededCallback,failedCallback,userContext); },
GetSWBAT:function(prefixText,count,succeededCallback, failedCallback, userContext) {
return this._invoke(this._get_path(), 'GetSWBAT',false,{prefixText:prefixText,count:count},succeededCallback,failedCallback,userContext); }}
WebService.registerClass('WebService',Sys.Net.WebServiceProxy);
WebService._staticInstance = new WebService();
WebService.set_path = function(value) { WebService._staticInstance.set_path(value); }
WebService.get_path = function() { return WebService._staticInstance.get_path(); }
WebService.set_timeout = function(value) { WebService._staticInstance.set_timeout(value); }
WebService.get_timeout = function() { return WebService._staticInstance.get_timeout(); }
WebService.set_defaultUserContext = function(value) { WebService._staticInstance.set_defaultUserContext(value); }
WebService.get_defaultUserContext = function() { return WebService._staticInstance.get_defaultUserContext(); }
WebService.set_defaultSucceededCallback = function(value) { WebService._staticInstance.set_defaultSucceededCallback(value); }
WebService.get_defaultSucceededCallback = function() { return WebService._staticInstance.get_defaultSucceededCallback(); }
WebService.set_defaultFailedCallback = function(value) { WebService._staticInstance.set_defaultFailedCallback(value); }
WebService.get_defaultFailedCallback = function() { return WebService._staticInstance.get_defaultFailedCallback(); }
WebService.set_enableJsonp = function(value) { WebService._staticInstance.set_enableJsonp(value); }
WebService.get_enableJsonp = function() { return WebService._staticInstance.get_enableJsonp(); }
WebService.set_jsonpCallbackParameter = function(value) { WebService._staticInstance.set_jsonpCallbackParameter(value); }
WebService.get_jsonpCallbackParameter = function() { return WebService._staticInstance.get_jsonpCallbackParameter(); }
WebService.set_path("/esm/WebService.asmx");
WebService.Getregid= function(prefixText,count,onSuccess,onFailed,userContext) {WebService._staticInstance.Getregid(prefixText,count,onSuccess,onFailed,userContext); }
WebService.doCycleDays= function(startdate,enddate,cycledays,onSuccess,onFailed,userContext) {WebService._staticInstance.doCycleDays(startdate,enddate,cycledays,onSuccess,onFailed,userContext); }
WebService.doStartDate= function(startdate,cycledays,onSuccess,onFailed,userContext) {WebService._staticInstance.doStartDate(startdate,cycledays,onSuccess,onFailed,userContext); }
WebService.GetTowns= function(prefixText,onSuccess,onFailed,userContext) {WebService._staticInstance.GetTowns(prefixText,onSuccess,onFailed,userContext); }
WebService.GetDistricts= function(prefixText,onSuccess,onFailed,userContext) {WebService._staticInstance.GetDistricts(prefixText,onSuccess,onFailed,userContext); }
WebService.GetCounty= function(prefixText,onSuccess,onFailed,userContext) {WebService._staticInstance.GetCounty(prefixText,onSuccess,onFailed,userContext); }
WebService.GetIM= function(onSuccess,onFailed,userContext) {WebService._staticInstance.GetIM(onSuccess,onFailed,userContext); }
WebService.GetSWBAT= function(prefixText,count,onSuccess,onFailed,userContext) {WebService._staticInstance.GetSWBAT(prefixText,count,onSuccess,onFailed,userContext); }
