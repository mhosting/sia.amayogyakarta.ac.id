<?
include_once("reference/config.php");
$ta=$_GET['ta'];
$nim=$_GET['nim'];
?>
<html><head><title>Cetak KRS</title>

<meta http-equiv="pragma" content="no-cache">
<style type="text/css" media="all">
body {
	font: 12pt "Times New Roman";
	background-color: white;
}
#navigasi{
	font-family: Tahoma, Verdana;
	border: 1px solid #A8A8A8;
	padding: 10px;
	width: 140mm;
	text-align: center;
	background-color: #f5f5f5;
}
#navigasi #klik{
	border: 1px solid ButtonText;
	color: ButtonText;
	font-weight: bold;
	background-color: #EEEEEE;
	padding-left: 5px;
	padding-right: 5px;
}
#navigasi input.tombol{
	border: 1px solid ButtonText;
	color: ButtonText;
}
.paperA4{
	height: 175mm;
	width: 247mm;
	border: 1px dashed #66ff66;
	padding: 1px;
	font-family: "Trebuchet MS", Arial, Tahoma, Verdana;
}
.papersetengahA4{
	height: 147mm;
	width: 192mm;
	border: 1px dashed #66ff66;
	padding: 1px;
	font-family: "Trebuchet MS", Arial, Tahoma, Verdana;
	background-color: White;
}
.header1{
	font-size: 13px; font-family:Arial, Helvetica, sans-serif; font-weight:bold;
}
.header2{
	font-size: 24px; font-family: "Rockwell Extra Bold", "Arial Black";
	font-weight:bold;
}
.alamat{
	font-size: 10px; font-family:Arial, Helvetica, sans-serif;
}
.fotoTD{
	padding: 0px;
}
.fotoDIV{
	font-size: 10px;
	width:20mm;
	height: 20mm;
	border: #000000 solid 1px;
	text-align: center;
	vertical-align:  baseline;
}
.garis{ border-bottom: 3px solid Black; }
.judulKRS{ font-family: Arial, Helvetica, sans-serif;  font-size: 18px;  font-weight: bold;   }
.identitas{	font-family: Arial, Helvetica, sans-serif; font-size: 11px;}
.tabelkrs{
	/*font-family: Arial, Tahoma, Verdana;*/
	font-family: Arial, Tahoma;
	font-size: 9px;
	border: 0px solid #000000;
	border-right: 0px solid #000000;
}
.tabelkrs .thl, .thc{
	border-left: 1px solid #000000;
	border-top: 1px solid #000000;
	border-bottom: 2px solid #000000;
}
.tabelkrs .thr{
	border-left: 1px solid #000000;
	border-right: 1px solid #000000;
	border-top: 1px solid #000000;
	border-bottom: 4px solid #000000;
}
.tabelkrs .thct{
	border-left: 1px solid #000000;
	border-top: 1px solid #000000;
	border-bottom: 0px solid #000000;
}
.tabelkrs .thrt{
	border-left: 1px solid #000000;
	border-top: 1px solid #000000;
	border-bottom: 0px solid #000000;
	border-right: 1px solid #000000;
}
.tabelkrs .thcb{
	border-left: 1px solid #000000;
	border-top: 1px solid #000000;
	border-bottom: 2px solid #000000;
}
.tabelkrs .thrb{
	border-left: 1px solid #000000;
	border-top: 1px solid #000000;
	border-bottom: 2px solid #000000;
	border-right: 1px solid #000000;
}
.tabelkrs .tdl, .tdc, .tdl2, .tdc2{
	border-left: 1px solid #000000;
	border-bottom: 1px solid #000000;
}
.tabelkrs .tdr{
	border-left: 1px solid #000000;
	border-right: 1px solid #000000;
	border-bottom: 1px solid #000000;
}
.tabelIndek{
	font-family: Arial, Tahoma; 
	font-size: 10px;
}
.ttd{
	font-size: 12px;
	text-align: center;
}
.tabelIndek .content{
	font-size: 10px;
	color: Black;
}
.catatan{
	font-size: 10px;
}</style>
<style>
body {
	font: 12pt "Times New Roman";
	background-color: white;
}
#navigasi{
	font-family: Tahoma, Verdana;
	border: 1px solid #A8A8A8;
	padding: 10px;
	width: 140mm;
	text-align: center;
	background-color: #f5f5f5;
}
#navigasi #klik{
	border: 1px solid ButtonText;
	color: ButtonText;
	font-weight: bold;
	background-color: #EEEEEE;
	padding-left: 5px;
	padding-right: 5px;
}
#navigasi input.tombol{
	border: 1px solid ButtonText;
	color: ButtonText;
}
.paperA4{
	height: 175mm;
	width: 247mm;
	border: 1px dashed #66ff66;
	padding: 1px;
	font-family: "Trebuchet MS", Arial, Tahoma, Verdana;
}
.papersetengahA4{
	height: 147mm;
	width: 192mm;
	border: 1px dashed #66ff66;
	padding: 1px;
	font-family: "Trebuchet MS", Arial, Tahoma, Verdana;
	background-color: White;
}
.header1{
	font-size: 13px; font-family:Arial, Helvetica, sans-serif; font-weight:bold;
}
.header2{
	font-size: 24px; font-family: "Rockwell Extra Bold", "Arial Black";
	font-weight:bold;
}
.alamat{
	font-size: 10px; font-family:Arial, Helvetica, sans-serif;
}
.fotoTD{
	padding: 0px;
}
.fotoDIV{
	font-size: 10px;
	width:20mm;
	height: 20mm;
	border: #000000 solid 1px;
	text-align: center;
	vertical-align:  baseline;
}
.garis{ border-bottom: 3px solid Black; }
.judulKRS{ font-family: Arial, Helvetica, sans-serif;  font-size: 18px;  font-weight: bold;   }
.identitas{	font-family: "MS Sans Serif", Arial, Tahoma; font-size: 11px;}
.tabelkrs{
	/*font-family: Arial, Tahoma, Verdana;*/
	font-family: Arial, Tahoma;
	font-size: 9px;
	border: 0px solid #000000;
	border-right: 0px solid #000000;
}
.tabelkrs .thl, .thc{
	border-left: 1px solid #000000;
	border-top: 1px solid #000000;
	border-bottom: 2px solid #000000;
}
.tabelkrs .thr{
	border-left: 1px solid #000000;
	border-right: 1px solid #000000;
	border-top: 1px solid #000000;
	border-bottom: 4px solid #000000;
}
.tabelkrs .thct{
	border-left: 1px solid #000000;
	border-top: 1px solid #000000;
	border-bottom: 0px solid #000000;
}
.tabelkrs .thrt{
	border-left: 1px solid #000000;
	border-top: 1px solid #000000;
	border-bottom: 0px solid #000000;
	border-right: 1px solid #000000;
}
.tabelkrs .thcb{
	border-left: 1px solid #000000;
	border-top: 1px solid #000000;
	border-bottom: 2px solid #000000;
}
.tabelkrs .thrb{
	border-left: 1px solid #000000;
	border-top: 1px solid #000000;
	border-bottom: 2px solid #000000;
	border-right: 1px solid #000000;
}
.tabelkrs .tdl, .tdc, .tdl2, .tdc2{
	border-left: 1px solid #000000;
	border-bottom: 1px solid #000000;
}
.tabelkrs .tdr{
	border-left: 1px solid #000000;
	border-right: 1px solid #000000;
	border-bottom: 1px solid #000000;
}
.tabelIndek{
	font-family: Arial, Tahoma; 
	font-size: 10px;
}
.ttd{
	font-size: 12px;
	text-align: center;
}
.tabelIndek .content{
	font-size: 10px;
	color: Black;
}
.catatan{
	font-size: 10px;
}</style></head><body onload="document.getElementById('currPos').focus();">
<div id="navigasi">

<font face="Lucida Sans">
<?
							$CEKXX=substr($ta,0,4);
							$CEKXX2=substr($ta,4,1);
							$CEKXXX=$CEKXX+1;
							$P=substr($ta,4,1);
							if(($P%2)==1)
							{
							$SS="GANJIL";
							$LALUX=($CEKXX-1);
							$SP="2";
							$LALU="$LALUX$SP";
							$ASLI="GENAP";
							
							$NEXTX=$CEKXX;
							$SPX="2";
							$NEXT="$NEXTX$SPX";
							}else
							{
							$SS="GENAP";
							$LALUX=$CEKXX;
							$SP="1";
							$LALU="$LALUX$SP";
							$ASLI="GANJIL";
							
							$NEXTX=($CEKXX+1);
							$SPX="1";
							$NEXT="$NEXTX$SPX";
							}	
$hasilv = mysql_query("select k.nmkonsen from msmhs m,konsentrasi k where m.NIMHSMSMHS = '$nim' and k.kdkonsen=m.kdkonsen");
$data2 = mysql_fetch_array($hasilv);
$nama_konsen=$data2['nmkonsen'];
$qall = "SELECT *,m.KDJENMSMHS as jenjang,m.TPLHRMSMHS as tempatLahir,DAY(m.TGLHRMSMHS) as tanggalLahir,MONTH(m.TGLHRMSMHS) as bulanLahir,YEAR(m.TGLHRMSMHS) as tahunLahir FROM msmhs m,kelasparalel_mhs k,mspst ms where ms.KDPSTMSPST=m.KDPSTMSMHS and k.nimhs=m.NIMHSMSMHS and m.NIMHSMSMHS='$nim'";
		$hasilall = mysql_query($qall);   
				while($dataall = mysql_fetch_array($hasilall))
		{
			$nimnya= $dataall["NIMHSMSMHS"];
			$nama= $dataall["NMMHSMSMHS"];
			$KDPSTMSMHS= $dataall["KDPSTMSMHS"];
			$kelas= $dataall["nmkelas"];
			$NMPSTMSPST= $dataall["NMPSTMSPST"];
			$tempatlahir=$dataall["TPLHRMSMHS"];
			$tgl=$dataall['tanggalLahir'];
			$bulan=$dataall['bulanLahir'];
			$tahun=$dataall['tahunLahir'];
			$nama_prodi=$dataall['NMPSTMSPST'];
			$KELAS=$kelas;
			//$jenjang=$dataall['jenjang'];
			
$array_bulan=array("Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","Nopember","Desember");
$bulannya=$array_bulan[($bulan-1)];
			}
			
			$MASUK=substr($KELAS,5,2);
							$pecahkelas=explode("/",$KELAS); 
							$kelas1=strtoupper($pecahkelas[0]); // buat huruf besar semua 
							$kelas2=strtoupper($pecahkelas[1]);
						
							$TAHUN="20$MASUK";
							$ASALXX="20$MASUK";
							$ASALXX2="1";
							$CEKXX=substr($ta,0,4);
							$CEKXX2=substr($ta,4,1);
							$CEKXXX=$CEKXX+1;
							if($ASALXX2==$CEKXX2)
							{
							$P=(($CEKXX-$ASALXX)*2)+1;
							}else
							{
							$P=(($CEKXX-$ASALXX)*2)+2;
							}

							if($P==1)
							{
							$R="I";
							$V=1;
							}elseif($P==2)
							{
							$R="II";
							$V=2;
							}elseif($P==3)
							{
							$R="III";
							$V=3;
							}elseif($P==4)
							{
							$R="IV";
							$V=4;
							}elseif($P==5)
							{
							$R="V";
							$V=5;
							}elseif($P==6)
							{
							$R="VI";
							$V=6;
							}elseif($P==7)
							{
							$R="VII";
							$V=7;
							}elseif($P==8)
							{
							$R="VIII";
							$V=8;
							}elseif($P==9)
							{
							$R="IX";
							$V=9;
							}elseif($P==10)
							{
							$R="X";
							$V=10;
							}elseif($P==11)
							{
							$R="XI";
							$V=11;
							}elseif($P==12)
							{
							$R="XII";
							$V=12;
							}elseif($P==13)
							{
							$R="XIII";
							$V=13;
							}elseif($P==14)
							{
							$R="XIV";
							$V=14;
							}elseif($P==15)
							{
							$R="XV";
							$V=15;
							}else
							{
							$R="??";
							}
								$perintah=mysql_query("select * from kelasparalel k,msdos t where k.namakelas='$kelas' and t.NODOSMSDOS=k.nodos"); 
							$datanya=mysql_fetch_array($perintah);
							$namados=$datanya['NMDOSMSDOS'];
							$namakelas=$datanya['namakelas'];
							$pecahnm=explode("/",$namakelas); 
							$kelasa=strtoupper($pecahnm[0]); // buat huruf besar semua 
							$kelasb=strtoupper($pecahnm[1]);
							$kelasc=strtoupper($pecahnm[2]);
							
							$kdkonsen=$datanya['kdkonsen'];
							if($kdkonsen=="OF")
							{
							$JUR="61401";
							$KONSEN="MANAJEMEN ADMINISTRASI OBAT DAN FARMASI";
							$KODEX="33";
							}elseif($kdkonsen=="RS")
							{
							$JUR="61401";
							$KODEX="03";
							$KONSEN="MANAJEMEN ADMINISTRASI RUMAH SAKIT";
							}
							elseif($kdkonsen=="TU")
							{
							$JUR="61401";
							$KODEX="13";
							$KONSEN="MANAJEMEN ADMINISTRASI TRANSPORTASI UDARA";
							}
							$PRODI="MANAJEMEN ADMINISTRASI";
							
							if($kelasc>=15)
							{
							$KODEX="15";	
								
							}
?>
</font>
<form name="posT" method="post">


<?
if($KODEX=="03")
					{
?>


<!-- BEGIN : navigasi THSMS -->
<font face="Lucida Sans"><a href="cetak_krs_ok.php?ta=<? print($LALU); ?>&nim=<? print($nim); ?>" id="klik" style="text-decoration: none;"><? print($LALU); ?> <font size="5">&#8592;</font></a>&nbsp;&nbsp;&nbsp;
<input id="currPosTHSMS" name="currPosTHSMS" value="<? print($ta); ?>" size="10" maxlength="5" style="text-align: center; font-family: Verdana,Arial,Helvetica,sans-serif; font-size: 14px;" type="text">
&nbsp;&nbsp;&nbsp;
<a href="cetak_krs_ok.php?ta=<? print($NEXT); ?>&nim=<? print($nim); ?>" id="klik" style="text-decoration: none;"><font size="5">&#8594;</font> <? print($NEXT); ?></a><input name="go" value="GO" style="display: none;" onclick="return executeNewThsms();" type="submit">

<!-- END : navigasi THSMS -->

</font>
<form name="posN" method="post">
<!-- BEGIN : navigasi NIM -->
<font face="Lucida Sans"><br>
<a href="cetak_krs_ok.php?ta=<? print($ta); ?>&nim=0<? print($nim-1); ?>" id="klik" style="text-decoration: none;">0<? print($nim-1); ?> <font size="5">&#8592;</font></a>&nbsp;&nbsp;&nbsp;
<input id="currPos" name="currPos" value="<? print($nim); ?>" size="10" maxlength="10" style="text-align: center; font-family: Verdana,Arial,Helvetica,sans-serif; font-size: 14px;" type="text">
&nbsp;&nbsp;&nbsp;
<a href="cetak_krs_ok.php?ta=<? print($ta); ?>&nim=0<? print($nim+1); ?>" id="klik" style="text-decoration: none;"><font size="5">&#8594;</font> 0<? print($nim+1); ?></a>
<input name="go" value="GO" style="display: none;" onclick="return executeNewNim();" type="submit">

<!-- END : navigasi NIM -->

<?
				
					}else
{
?>

<!-- BEGIN : navigasi THSMS -->
<font face="Lucida Sans"><a href="cetak_krs_ok.php?ta=<? print($LALU); ?>&nim=<? print($nim); ?>" id="klik" style="text-decoration: none;"><? print($LALU); ?> <font size="5">&#8592;</font></a>&nbsp;&nbsp;&nbsp;
<input id="currPosTHSMS" name="currPosTHSMS" value="<? print($ta); ?>" size="10" maxlength="5" style="text-align: center; font-family: Verdana,Arial,Helvetica,sans-serif; font-size: 14px;" type="text">
&nbsp;&nbsp;&nbsp;
<a href="cetak_krs_ok.php?ta=<? print($NEXT); ?>&nim=<? print($nim); ?>" id="klik" style="text-decoration: none;"><font size="5">&#8594;</font> <? print($NEXT); ?></a><input name="go" value="GO" style="display: none;" onclick="return executeNewThsms();" type="submit">

<!-- END : navigasi THSMS -->

</font>
<form name="posN" method="post">
<!-- BEGIN : navigasi NIM -->
<font face="Lucida Sans"><br>
<a href="cetak_krs_ok.php?ta=<? print($ta); ?>&nim=<? print($nim-1); ?>" id="klik" style="text-decoration: none;"><? print($nim-1); ?> <font size="5">&#8592;</font></a>&nbsp;&nbsp;&nbsp;
<input id="currPos" name="currPos" value="<? print($nim); ?>" size="10" maxlength="10" style="text-align: center; font-family: Verdana,Arial,Helvetica,sans-serif; font-size: 14px;" type="text">
&nbsp;&nbsp;&nbsp;
<a href="cetak_krs_ok.php?ta=<? print($ta); ?>&nim=<? print($nim+1); ?>" id="klik" style="text-decoration: none;"><font size="5">&#8594;</font> <? print($nim+1); ?></a>
<input name="go" value="GO" style="display: none;" onclick="return executeNewNim();" type="submit">

<!-- END : navigasi NIM -->
<?
}
?>



</font><div id="alertMsg" align="left"></div>
<hr noshade="noshade">
<div style="text-align: left;">
<font face="Lucida Sans"><input class="tombol" value="Kembali" onclick="window.location.href='index.php?route=cetak_krs&ta=<? print($ta); ?>&nim=<? print($nim); ?>'" type="button">
<input class="tombol" value="Cetak KRS" onclick="printOutKRS();" type="button">
</font></div></form></form></div>


<font face="Lucida Sans"><script language="javascript" type="text/javascript">


function executeNewNim(){
	var newNim = document.getElementById('currPos');
	var alertMsg = document.getElementById('alertMsg');
	if(newNim.value!="<? print($nim); ?>"){
		document.posN.action='?ta=<? print($ta); ?>&nim='+newNim.value;
	}else{
		alertMsg.innerHTML = '<br>Masukkan NIM yang berbeda. Kemudian tekan ENTER!<br>';
		return false;
	}
}

function executeNewThsms(){
	var newThsms = document.getElementById('currPosTHSMS');
	var alertMsg = document.getElementById('alertMsg');
	if(newThsms.value!="<? print($ta); ?>"){
		document.posT.action='?ta='+newThsms.value+'&nim=<? print($nim); ?>';
	}else{
		alertMsg.innerHTML = '<br>Masukkan Tahun Ajaran yang berbeda. Kemudian tekan ENTER!<br>';
		return false;
	}
}

function printOutKRS(){
	var getDisplay = document.getElementById("printarea").innerHTML;	
	var setPrint = window.open("","printed","width=740,height=570,menubar=0,scrollbars=1,statusbar=1");
	
	setPrint.document.open();
	setPrint.document.write('<html>');
	setPrint.document.write('<head>');
	setPrint.document.write('<meta http-equiv="pragma" content="no-cache">');
	setPrint.document.write('<title>Print Preview</title>');
	setPrint.document.write('<link href="include/cetakkrs_preview.css" rel="stylesheet" type="text/css">');
	//setPrint.document.write('<style>');
	//setPrint.document.write('</style>');
	setPrint.document.write('</head>');
	setPrint.document.write('<body onLoad="self.print()">');
	setPrint.document.write(getDisplay);
	setPrint.document.write('</body></html>');
	setPrint.document.close();
	//setPrint.window.close("","printed");
}

function numeralsOnly(evt) {
    evt = (evt) ? evt : event;
    var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode : 
        ((evt.which) ? evt.which : 0));
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

</script>
<?

			?>
</font><form name="frm" method="post">
<table border="0" bordercolor="#0000cc" cellpadding="0" cellspacing="0">
	<tbody><tr valign="top">
		<td style="padding: 1cm;">
	
	<div id="printarea" class="papersetengahA4">
	
		<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
			<tbody><tr height="70" valign="top">
				<td class="garis" align="center" width="100"><img src="include/logoama.png" class="logo" style="height:70px;border-width:0px;"></td>
				<td colspan="2" class="garis">
					<span class="header1">AKADEMI MANAJEMEN ADMINISTRASI</span><br>
					<span class="header2">YOGYAKARTA</span> <br>
					<span class="alamat"> 
						Kampus: Jalan Pramuka No 70-85B Yogyakarta Telp/Fax. 0274-4340658<br>
						Website: http://www.amayogyakarta.ac.id, Email: info@amayogyakarta.ac.id
						
											</span>
				</td>
		        <td class="fotoTD" align="right" valign="middle" width="40"><div class="fotoDIV"> foto<br>2�3</div></td>
			</tr>
			<tr height="50">
				<td colspan="4" class="judulKRS" align="center">KARTU RENCANA STUDI (KRS)</td>
			</tr>
			<tr>
				<td colspan="4" height="8"></td>
			</tr>
			<tr valign="top">
			  <td colspan="4" align="center">
										<table border="0" cellpadding="0" cellspacing="0" width="100%">
						<tbody><tr valign="top">
							<td width="14%">N A M A</td><td class="identitasItem" width="1%">:</td><td width="250"><strong><? print($nama); ?></strong></td>
							<td width="14%">No. Mahasiswa</td><td class="identitasItem">:</td><td width="200"><strong><? print($nim); ?></strong></td>
						</tr>
						<tr valign="top">
							<td class="identitasItem">Program Studi</td><td class="identitasItem">:</td><td><strong>D3 - <? print($nama_prodi); ?></strong></td>
							<td class="identitasItem">Kelas - Semester</td><td class="identitasItem" width="1%">:</td><td><strong><? print($kelas); ?>-<? print($R); ?></strong></td>
						</tr>
						<tr valign="top">
																			
																	<td class="identitasItem">Konsentrasi</td><td class="identitasItem">:</td><td><strong><? print($nama_konsen); ?></strong></td>
																	
							<td class="identitasItem">Dosen Wali</td><td class="identitasItem">:</td><td><strong><? print($namados); ?></strong></td>
						</tr>
					</tbody></table>			  
				</td>
			</tr>
			<tr>
				<td colspan="4" height="10"></td>
			</tr>
			<tr>
				<td colspan="4" valign="top">
										
							<table class="tabelkrs" bgcolor="#ffffff" border="0" cellpadding="3" cellspacing="0" width="100%">
								<tbody><tr bgcolor="#ffffff" valign="middle">
									<th class="thl" rowspan="2" width="2%">No.</th>
									<th class="thc" rowspan="2" width="8%">KODE</th>
									<th class="thc" rowspan="2">MATA KULIAH</th>
									<th class="thc" rowspan="2" width="5%">SKS</th>
									<th class="thc" rowspan="2" width="25%">Nama Dosen</th>
									<th class="thct" colspan="2">UJIAN MID</th>
									<th class="thrt" colspan="2">UJIAN AKHIR</th>
								</tr>
								<tr bgcolor="#ffffff" valign="middle">
								  <th class="thc" width="6%">Tgl.</th>
								  <th class="thc" width="4%">Paraf</th>
								  <th class="thcb" width="6%">Tgl.</th>
								  <th class="thrb" width="4%">Paraf</th>
							  </tr>
							  
							  <?
	$no = 0;
	$sks2 = 0;
	$totalambil=0;
	$hasilt = mysql_query("select * from trnlm where NIMHSTRNLM='$nim' and THSMSTRNLM='$ta' order by KDKMKTRNLM ASC");				
				while ($datat = mysql_fetch_array($hasilt))
				{						
					//warna pada kolom
					$no++;
				$KDKMKTRNLM=$datat['KDKMKTRNLM'];
				$THSMSTRNLM=$datat['THSMSTRNLM'];
				$NLAKHTRNLM=$datat['NLAKHTRNLM'];
				$ulang=$datat['ulang'];
				$totmk2= "SELECT m.SKSMKTBKMK,m.NAKMKTBKMK,m.SEMESTBKMK from tbkmk m where m.KDKMKTBKMK='$KDKMKTRNLM' and m.THSMSTBKMK='$THSMSTRNLM' and m.KDPSTTBKMK='$JUR' and (kdkonsen='u' or kdkonsen='$kdkonsen')";
											  $hasilmk2 = mysql_query($totmk2);
											 
											 $datamk2 = mysql_fetch_array($hasilmk2);
											  $sks2= $datamk2["SKSMKTBKMK"];
											    $namamk= $datamk2["NAKMKTBKMK"];
									
												$SEMESTBKMK= $datamk2["SEMESTBKMK"];
												$totalambil=$totalambil+$sks2;
							
							?>
							<tr bgcolor="#ffffff">
										<td class="tdl" align="center"><? print("$no"); ?></td>
										<td class="tdc" align="center"><? print("$KDKMKTRNLM"); ?></td>
										<?
										if($ulang==1)
										{
										?>
											<td class="tdc"><i><? print($namamk); ?></i> - <b><i>*) Ulang</i></b></td>
										<?
										}else
										{
											?>
											<td class="tdc"><? print($namamk); ?></td>
										<?
										}
										?>
									
										<td class="tdc" align="center"><? print($sks2); ?></td>
										<td class="tdc">&nbsp;</td>
										<td class="tdc">&nbsp;</td>
										<td class="tdc">&nbsp;</td>
										<td class="tdc" align="center">&nbsp;</td>
										<td class="tdr" align="right">&nbsp;</td>
									</tr>
								
							<?
							
							}

							if ($no < 1 ){
							?>
					<tr bgcolor="#ffffff" valign="middle">
					<td class="tdl2" colspan="9" align="center"><strong>TIDAK KRS</strong></td>
         
      </tr>
						 <?
				} else {
				$hasilt = mysql_query("select * from trakm where NIMHSTRAKM='$nim' and THSMSTRAKM='$LALU' order by THSMSTRAKM DESC limit 0,1");				
				while ($datat = mysql_fetch_array($hasilt))
				{						
					//warna pada kolom
		
				$THSMSTRAKM2=$datat['THSMSTRAKM'];
				$skstot=$datat['SKSTTTRAKM'];
				$ipksem=$datat['NLIPKTRAKM'];
				}
				

$engDate=date("l F d, Y H:i:s A");
//echo "English Date : ". $engDate ."<p>";

switch (date("w")) {
case "0" : $hari="Minggu";break;
case "1" : $hari="Senin";break;
case "2" : $hari="Selasa";break;
case "3" : $hari="Rabu";break;
case "4" : $hari="Kamis";break;
case "5" : $hari="Jumat";break;
case "6" : $hari="Sabtu";break;
} switch (date("m")) {
case "1" : $bulan="Januari";break;
case "2" : $bulan="Februari";break;
case "3" : $bulan="Maret";break;
case "4" : $bulan="April";break;
case "5" : $bulan="Mei";break;
case "6" : $bulan="Juni";break;
case "7" : $bulan="Juli";break;
case "8" : $bulan="Agustus";break;
case "9" : $bulan="September";break;
case "10" : $bulan="Oktober";break;
case "11" : $bulan="November";break;
case "12" : $bulan="Desember";break;
}
$indDate="". date("d") ." $bulan". date(" Y");

?>
			
					
									
																<tr bgcolor="#ffffff" valign="middle">
								<td class="tdl2" colspan="3" align="center"><strong>J U M L A H</strong></td>
								<td class="tdc2" align="center"><strong><? print($totalambil); ?>&nbsp;</strong></td>
								<td class="tdc2">&nbsp;</td>
								<td class="tdc" colspan="2" rowspan="2">&nbsp;</td>
								<td class="tdr" colspan="2" rowspan="2">&nbsp;</td>
							</tr>
							<tr bgcolor="#ffffff" valign="middle">
							  <td class="tdl2" colspan="5" align="center"><strong>PENGESAHAN</strong></td>
						  </tr>
					</tbody></table>
														<div class="tabelIndek">
								Beban studi yang telah ditempuh / Beban studi sampai lulus : <strong class="content"><? print($skstot); ?></strong> / <strong class="content">159</strong>
								<br>
								Indek Prestasi Komulatif (IPK) semester yang lalu : <strong><? print($ipksem); ?></strong>
								
								</div>
				</td>
			</tr>
					
			<tr valign="bottom">
				<td colspan="4" height="1%" valign="bottom">
				
					<table class="ttd" bgcolor="#000000" border="0" cellpadding="3" cellspacing="0" width="100%">
						<tbody><tr align="left" bgcolor="#ffffff" valign="bottom">
							<td style="text-align: center;" width="30%">
						  			
							</td>
					      	<td>&nbsp;</td>
							<td style="text-align: center;" width="30%">
																Yogyakarta, <? echo "". $indDate ."";?>								<br>
								Mahasiswa,
								<br><br><br><br>
								( <b><? print($nama); ?></b> )	
							</td>
						</tr>
					</tbody></table>
					
				</td>			
			</tr>
						 <?
				}
							?>
							  
										
																		
																		
																		
														
							
							
							
							
							
				
		</tbody></table>			

<!--</td><td>

<hr noshade="noshade" />
<input type="button" value="Kembali" onClick="window.location.href='cetakkrs.php?ta=20082~04061465'">
<input type="button" value="Cetak KRS" onClick="printOutKRS();" >
</form>		
<br>
<iframe name="simpankhs" id="simpankhs" frameborder="0" scrolling="no" width="300" height="100%"></iframe>
-->
</div></td></tr></tbody></table>

</form></body></html>
