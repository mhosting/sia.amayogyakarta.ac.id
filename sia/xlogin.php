<!DOCTYPE html>
<html lang="en">
<head>
    <title>SISTEM INFORMASI KAMPUS</title>
    <meta charset="utf-8">
    <!-- Global stylesheets -->
    <link href="source/min00000.css" rel="stylesheet" type="text/css"/>
    <link href="source/special-.css" rel="stylesheet" type="text/css"/> 
    <script type="text/javascript" src="source/min00000.js" />
    <script type="text/javascript" src="source/standard.js"></script>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
</head>

<body class="special-page login-bg dark">

     <section id="message">
    <div class="block-border">
        <div class="block-content no-title dark-bg">
            <p class="mini-infos">
                <strong>Sistem Informasi Kampus</strong></p>
        </div>
    </div>
    </section>
    <section id="login-block">
    <form name="loginform1" method="post" action="" id="loginform1" class="form with-margin">
<div>
<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwULLTExNzg0NTM4NDYPZBYCAgEPZBYCAgMPZBYCZg9kFgICAQ8WAh4HVmlzaWJsZWhkZA==" />
</div>

<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['loginform1'];
if (!theForm) {
    theForm = document.loginform1;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>


<script src="/WebResource.axd?d=Rp-GMddNA5qFCXpq_fJtmJ0k3GsCVOh9K5PR2ipEVoQwEGWi1Coy5c6W3yLof7_Sddzn4Te9mbL6g1qj-trKx7u_5vFPRltXpEfN4S0PiYY1&amp;t=634502014198580000" type="text/javascript"></script>


<script src="/ScriptResource.axd?d=NrA3DcEmMKmr6M-cEN3WfJre-RFDG0AaMaXllwHpJ_-uVbTy_5SvlHSaKmntcxag40RI_TGcJU8pKEz-H5lN0fEa3rSbT4TglGBrYja6qzA4Nyl5AHcionFAvMr-JfjwjKDTmYBJRUqnNIgp1pFfeA2&amp;t=ffffffffbd2983fc" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
if (typeof(Sys) === 'undefined') throw new Error('ASP.NET Ajax client-side framework failed to load.');
//]]>
</script>

<script src="/ScriptResource.axd?d=2TXVY-ZqxXHHRyDcBiGb75SPbbnTYqr3JgvHf0r-72jrAiPvZYS3fP2SZwYPkJqDSjmET0OvzYMICA5gw-zaGMJsEu1xrLEMWI6sRgAJsQGRXVqsCz6iEw0lzmETAyfVdqemeTV4_DbQqRmX3M7iqA2&amp;t=ffffffffbd2983fc" type="text/javascript"></script>
    <script type="text/javascript">
//<![CDATA[
Sys.WebForms.PageRequestManager._initialize('ScriptManager1', 'loginform1', ['tUpdatePanel1',''], [], [], 90, '');
//]]>
</script>

    <div id="UpdatePanel1">
	
            <div class="block-border">
                <div class="block-content">
                   
                    <div class="block-header">
                        AMA YOGYAKARTA</div>
                    
                    <div id="siteUpdateProgress" style="display:none;">
		
                            <div class="TransparentGrayBackground">
                            </div>
                            <div class="Sample6PageUpdateProgress">
                                <img id="ajaxLoadNotificationImage2" src="source/arbo-loa.gif" alt="[image]" style="border-width:0px;" />
                                &nbsp;Validating Login...
                            </div>
                        
	</div>
                    <p>
                    </p>
                  <p id="msg" class="message error no-margin">Salah!</p>
                        <p class="inline-small-label">
                            <label for="login">
                                <span class="big">User name</span></label>
                            <input name="UserName" type="text" id="UserName" class="full-width" />
                        </p>
                        <p class="inline-small-label">
                            <label for="pass">
                                <span class="big">Password </span>
                            </label>
                            &nbsp;<input name="Password" type="password" id="Password" class="full-width" />
                        </p>
                        <input type="submit" name="button" value="Login" id="button" class="button float-right" />
                        <p class="input-height">

                        </p>
                        <p id="Abstract">
                        </p>
                        <fieldset class="grey-bg no-margin collapse">
                            <legend><a href="">Lupa password?</a></legend>
                            <p class="input-with-button">
                                <label for="recovery-mail">
                                    Silahkan Masukkan Username Anda</label>
                                <input name="tbemail" type="text" id="tbemail" />
                                <input type="submit" name="btnforgot" value="Send" id="btnforgot" class="button" />
                            </p>
                        </fieldset>
                    
                </div>
            </div>
        
</div>
    

<script type="text/javascript">
//<![CDATA[
Sys.Application.add_init(function() {
    $create(Sys.UI._UpdateProgress, {"associatedUpdatePanelId":"UpdatePanel1","displayAfter":500,"dynamicLayout":true}, null, null, $get("siteUpdateProgress"));
});
//]]>
</script>
</form>
    </section>
    <!--[if lt IE 8]></div><![endif]-->
    <!--[if lt IE 9]></div><![endif]-->
</body>
</html>

<!-- This document saved from http://esmdemo.calorisplanitia.com/esm/default.aspx -->
