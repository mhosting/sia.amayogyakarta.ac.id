
<script language="javascript" type="text/javascript" runat="server">

function date_valid_msg(caption, inp) {
	var s = caption + " harus diisi dengan urutan tgl-bln-thn.\nContoh input yang benar: 21-12-2006";
	if (inp.length < 10) return s;	
	else {		
		var temp = inp.split("-");
		var test = new Date(temp[1] + '/' + temp[0] + '/' + temp[2]);
		if (test == "Invalid Date") return s;
		else return "";
	}
}

function semester_valid_msg(caption,inp) {
	var s = caption + " harus diisi angka sepanjang 5 digit.\n";
	s += "Contoh input yang benar:\n";
	s += "20091 => T.A. 2009 Semester Ganjil\n";
	s += "20092 => T.A. 2009 Semester Genap";
	if (isNaN(inp) == "NaN") return s;
	else if (inp.length < 5) return s;
	else if (inp.substr(4,1) != "1"  && inp.substr(4,1) != "2") return s;
	else return "";
}

function blnthn_valid_msg(caption,inp) {
	var s = caption + " harus diisi angka sepanjang 6 digit.\n";
	s += "Contoh input yang benar:\n";
	s += "012005 => Bulan Januari 2005\n";
	s += "092006 => Bulan September 2006";
	if (isNaN(inp) == "NaN") return s;
	else if (inp.length < 6) return s;
	else return "";
}

function show_popup(url, w, h)
{
	if (w != null) var width = w; else var width = 500;
	if (h != null) var height = h; else var height = 700;
	var left = (screen.width-width)/2;
	var top = (screen.height-height)/2;
	window.open(url,'','top=' + top + ', scrollbars, left=' + left + ',height=' + height + ',width=' + width);
}

function show_popup2(url, w, h, target)
{
	if (w != null) var width = w; else var width = 500;
	if (h != null) var height = h; else var height = 700;
	var left = (screen.width-width)/2;
	var top = (screen.height-height)/2;
	window.open(url,target,'top=' + top + ', scrollbars, left=' + left + ',height=' + height + ',width=' + width);
}

function num_larger(inp, a) 
{
	var num = parseInt(inp);
	if (isNaN(inp)) return false;
	else if (num <= a) return false;
	else return true;
}

function nilai_huruf(txtField) /* =============== add by aji -- 26/02/2007 */
{
  	var checkOK = "ABCDET+-";
  	var checkStr = txtField.value;
  	var allValid = true;
  	var decPoints = 0;
  	var allNum = "";

  	for (i = 0;  i < checkStr.length;  i++)
	{
    	ch = checkStr.charAt(i);
    	for (j = 0;  j < checkOK.length;  j++)
      		if (ch == checkOK.charAt(j))
        		break;
    		if (j == checkOK.length)
			{
      			allValid = false;
      			break;
    		}
  		}

  		if (!allValid)
		{
			txtField.value = "";
			alert('Diisi huruf Kapital A / B / C / D / E / T \ndan/atau diikuti tanda (+) atau (-). \n\nContoh : A+, A, A-');
    		return (false);
  		}
	return (true);
}

function nilai_desimal(txtField) /* =============== add by aji -- 26/02/2007 */
{
  	var checkOK = "0123456789-.,";
  	var checkStr = txtField.value;
  	var allValid = true;
  	var decPoints = 0;
  	var allNum = "";

  	for (i = 0;  i < checkStr.length;  i++)
	{
    	ch = checkStr.charAt(i);
    	for (j = 0;  j < checkOK.length;  j++)
      		if (ch == checkOK.charAt(j))
        		break;
    		if (j == checkOK.length)
			{
      			allValid = false;
      			break;
    		}
    		if (ch == ".")
			{
      			allNum += ".";
      			decPoints++;
    		}
    		else if (ch != ",")
      			allNum += ch;
  		}

  		if (!allValid)
		{
			txtField.value = "0.00";
			alert('Diisi angka 0~9 dengan menggunakan pemisah desimal <u>titik</u>. Contoh : 4.00 ; 3.50 ; 2.55');
    		return (false);
  		}
	return (true);
}

// aji : 23/08/2007
function konfirm(msg,url){
	if(confirm(msg)){
		if(url){window.location.href=url;}
	}else{return false;}
}

</script>

<h1>Cetak KRS</h1>
                    <div class="block-controls">
                       
                    </div>
                    
                    <div class="no-margin"><br>
					<?


?>

</script>
<form name="frmt" method="get" action="cetak_krs_ok.php">
	
	 
      <table class="table" rules="all" onsortcommand="SortData" datakeyfield="ID" id="ctl00_ContentPlaceHolder2_dgAsgnm" style="background-color: rgb(239, 239, 239); border-color: Silver; border-width: 1px; border-style: solid; width: 100%; border-collapse: collapse;" align="Left" border="1" cellpadding="1" cellspacing="0">

   <tbody>
   
  
<tr class="c1">
         <td align="left" nowrap="nowrap">T.A/Semester</td>
         <td width="100%">
        <input class="textbox" name="ta" value="" id="f_thsms" size="4" maxlength="5" type="text">
			&nbsp;<span class="petunjuk"></span>
         	         </td>
      </tr>

<tr class="c1">
       <td class="label_req">NIM</td>
		<td>
			<input class="textbox_ro" name="nim" id="f_nimhs" size="15" value="" readonly="readonly" type="text">			
			
	
	<a href="javascript:show_popup('mhs_data.php', 600, 800)"><img src="include/lensa.png" title="Klik untuk mencari dari data Mahasiswa" border="0"></a>
		</td>
      </tr>
	   <tr class="c1">
				<td>Nama Mhs</td>
		<td><input class="textbox_ro" id="f_namamhs" size="30" readonly="readonly" type="text"></td>
			</tr>
 <tr class="c1">
<td></td>
		<td>
		
			<input class="tombol" value="Cetak KRS" title="Klik untuk Cetak KRS" onclick="document.frmt.exe.value='cetak'; return cek_input();" type="submit">
		</td>

     </tr>	   
<tr>
         <td colspan="2" class="contentfont">
		 		
			
      </tr>

         <tr>
         <td colspan="2" class="c4"></td>
      </tr>
	
</tbody></table>


<br><br>-
</form>

                    </div>
	  
   <script language="javascript">
function cek_tasms() {
	var f = document.frmt;
	var s = '';
	if (f.f_thsms.value == '') {
		s = "Thn.Akd/Semester belum Anda isi !";
		f.f_thsms.focus();
	}
	return s;
}

function cek_prodi() {
	var f = document.frmt;
	var s = '';
	if (f.f_jenpro.value == "") {
		s = "Program Studi belum Anda isi !";
		f.f_jenpro.focus();
	}
	return s;
}

function cek_nim() {
	var f = document.frmt;
	var s = '';
	if (s == "" && f.f_nimhs.value == "") {
		s = "NIM belum Anda isi !";
		f.f_nimhs.focus();
	}  
	return s;
}

function cek_input() {
	var f = document.frmt;
	var s = "";
	if (s == "") s = cek_tasms();
	//if (s == "") s = cek_prodi();
	if (s == "") s = cek_nim();
	if (s == "" && f.f_thsms.value != '') {
		s = semester_valid_msg("Input 'Thn.Akd/Semester' ", f.f_thsms.value);
		if (s != '') f.f_thsms.focus();
	}

	if (s != "") {
		alert(s)
		return false;
	} else
		return true;
}

function hook_mhs(kodejen,jenjang,kodepst,prodi,nim,nama) {
	document.frmt.f_nimhs.value = nim
	document.frmt.f_namamhs.value = nama
}
</script>