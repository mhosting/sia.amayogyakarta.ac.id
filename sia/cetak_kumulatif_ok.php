
<html>
<head>
<title>Cetak Salinan Transkip Kumulatif</title>
<style type="text/css" media="all">
body {
	font: 12pt "Times New Roman";
	background-color: white;
}
#navigasi{
	font-family: Tahoma, Verdana;
	border: 1px solid #A8A8A8;
	padding: 10px;
	width: 140mm;
	text-align: center;
	background-color: #f5f5f5;
}
#navigasi #klik{
	border: 1px solid ButtonText;
	color: ButtonText;
	font-weight: bold;
	background-color: #EEEEEE;
	padding-left: 5px;
	padding-right: 5px;
}
#navigasi input.tombol{
	border: 1px solid ButtonText;
	color: ButtonText;
}
.paperA4{
	width: 175mm;
	height: 247mm;
	border: 1px dashed #66ff66;
	padding: 1px;
	font-family: "Trebuchet MS", Arial, Tahoma, Verdana;
}
.papersetengahA4{
	width: 140mm;
	height: 185mm;
	border: 1px dashed #66ff66;
	padding: 1px;
	font-family: "Trebuchet MS", Arial, Tahoma, Verdana;
	background-color: White;
}
.header1{
	font-size: 16px; font-family:Arial, Helvetica, sans-serif; font-weight:bold;
}
.header2{
	font-size: 24px; font-family:Georgia, 'Times New Roman', Times, serif;
	font-weight:bold;
}
.alamat{
	font-size: 11px; font-family:Arial, Helvetica, sans-serif;
}
.garis{ border-bottom: 4px double Black; }
.judulKHS{ font-family: Arial, Helvetica, sans-serif;  font-size: 16px;  font-weight: bold;   }
.identitas{	font-family: "MS Sans Serif", Arial, Tahoma; font-size: 11px;}
.identitas2{	font-family: "MS Sans Serif", Arial, Tahoma; font-size: 14px;}
.tabelkhs{
	/*font-family: Arial, Tahoma, Verdana;*/
	font-family: Arial, Tahoma;
	font-size: 9px;
	border: 1px solid #000000;
	border-right: 2px solid #000000;
}
.tabelkhs .thl, .thc{
	border-left: 1px solid #000000;
	border-top: 1px solid #000000;
	border-bottom: 2px solid #000000;
}
.tabelkhs .thr{
	border-left: 1px solid #000000;
	border-right: 0px solid #000000;
	border-top: 1px solid #000000;
	border-bottom: 2px solid #000000;
}
.tabelkhs .thct{
	border-left: 1px solid #000000;
	border-top: 1px solid #000000;
	border-bottom: 0px solid #000000;
}
.tabelkhs .thcb{
	border-left: 1px solid #000000;
	border-top: 1px solid #000000;
	border-bottom: 2px solid #000000;
}
.tabelkhs .tdl, .tdc{
	border-left: 1px solid #000000;
	border-bottom: 1px solid #000000;
}
.tabelkhs .tdr{
	border-left: 1px solid #000000;
	border-right: 0px solid #000000;
	border-bottom: 1px solid #000000;
}
.tabelIndek{
	font-family: Arial, Tahoma; 
	font-size: 10px;
}
.tabelIndek .ttd{
	font-size: 12px;
	text-align: center;
}
.tabelIndek .rumuskhs{
	font-size: 10px;
}
.catatan{
	font-size: 10px;
}


body {
	background-color: #ffffff; /* background color */
	color: inherit; /* text color */
	font-family: Arial Narrow; /* font name */
	font-size: 14; /* font size */
	margin: 0px 0px 0px 0px; /* top right bottom left */
}

.phpreportmaker {
	color: inherit; /* text color */
	font-family: Verdana; /* font name */
	font-size: xx-small; /* font size */	
}

/* main table */
.ewTable {
	width: inherit; /* table width */	
	color: inherit; /* text color */
	font-family: Arial Narrow; /* font name */
	font-size: 10; /* font size */

	border-collapse: collapse;
}

/* main table data cells */
.ewTable td {
	padding: 3px; /* cell padding */
	
	border-color: #000000;  /* table background color */
}

/* main table header cells */
.ewTableHeader {
	background-color: #6699CC; /* header color */
	color: #FFFFFF; /* header font color */	
	vertical-align: top;	
}

.ewTableHeader a:link {	
	color: #FFFFFF; /* header font color */	
}

.ewTableHeader a:visited {	
	color: #FFFFFF; /* header font color */	
}

/* main table row color */
.ewTableRow {
	background-color: #FFFFFF;  /* alt row color 1 */
}

/* main table alternate row color */
.ewTableAltRow {
	background-color: #F5F5F5; /* alt row color 2 */	
}


/* group 1 */
.ewRptGrpHeader1 {
	background-color: #CCFFFF;
	font-weight: bold;		
}

.ewRptGrpField1 {
	background-color: #CCFFFF;
}

.ewRptGrpSummary1 {
	background-color: #BBEEEE;	
}

/* group 2 */
.ewRptGrpHeader2 {
	background-color: #CCFFCC;
	font-weight: bold;
}

.ewRptGrpField2 {
	background-color: #CCFFCC;
}

.ewRptGrpSummary2 {
	background-color: #BBEEBB;	
} 

/* group 3 */
.ewRptGrpHeader3 {
	background-color: #99FFCC;
	font-weight: bold;	
}

.ewRptGrpField3 {
	background-color: #99FFCC;
}

.ewRptGrpSummary3 {
	background-color: #88EEBB;	
}

/* group 4 */
.ewRptGrpHeader4 {
	background-color: #99FF99;
	font-weight: bold;	
}

.ewRptGrpField4 {
	background-color: #99FF99;
}

.ewRptGrpSummary4 {
	background-color: #88EE88;	
}

.ewRptGrpAggregate {
	font-weight: bold;
}

.ewRptPageSummary {
	background-color: #FFFFCC; /* page total background color */	
}

.ewRptGrandSummary {
	background-color: #FFFF66; /* grand total background color */	
}

/* classes for crosstab report only */

.ewRptColHeader {
	background-color: #CCFF66; /* column background color */
	font-weight: bold;
}

.ewRptColField {
	background-color: #CCFF66; /* column background color */
}



</style>
</head>
<body>
<?
include_once("reference/config.php");

$nim=$_POST['nim'];
$JUR=$_POST['prodi'];
$id_mhs=$nim;
echo '<form name="fr_input" method="post" action="index.php?route=khs">'
					.'<table width="100%" cellspacing="2" cellpadding="2">';
					?>
						<tr>
							<td style="border-bottom: 2px solid rgb(51, 51, 51); padding-bottom: 2px;" align="center" width="90">
							<img src="include/logoama.png" class="logo" style="height:70px;border-width:0px;"></td>
							<td colspan="7" style="border-bottom: 2px solid rgb(51, 51, 51); padding-bottom: 2px;">
								<span class="header1">AKADEMI MANAJEMEN ADMINISTRASI</span><br>
					<span class="header2">YOGYAKARTA</span> <br>
					<span class="alamat"> 
						Kampus: Jalan Pramuka No 70-85B Yogyakarta <br>Telp/Fax. 0274-4340658<br>
						Website: http://www.amayogyakarta.ac.id, Email: info@amayogyakarta.ac.id
						
											</span>
								</span>
							</td>
						</tr>
						<?
					echo '<tr>'
					.'<td colspan="8" height="35" align="center" class="judulKHS"><h2>Salinan Nilai Kumulatif</h2></td>'
					.'</tr></table>';
					$aturan = mysql_query("select statuskrs,tahun,prodi,ipkaktif from config");
	$dataaturan = mysql_fetch_array($aturan);
	$tahunajaran=$dataaturan['tahun'];	
	$ipkaktif=$dataaturan['ipkaktif'];	
	
					$qall = "SELECT *,DATE_FORMAT(m.TGLHRMSMHS,'%d-%m-%Y') AS tgl_lahir FROM msmhs m,kelasparalel_mhs k where k.nimhs=m.NIMHSMSMHS and m.NIMHSMSMHS='$nim'";		

		$hasilall = mysql_query($qall);
		$data = mysql_fetch_array($hasilall);

		
							$hasilv = mysql_query("select k.nmkonsen,k.kdkonsen from msmhs m,konsentrasi k where m.NIMHSMSMHS = '$id_mhs' and k.kdkonsen=m.kdkonsen");
							$datav = mysql_fetch_array($hasilv);
							$hasil2 = mysql_query("select nmkelas from kelasparalel_mhs where nimhs = '$id_mhs'");
							$data2 = mysql_fetch_array($hasil2);
					
							$nmkonsen=$datav['nmkonsen'];
							$KELAS=$data2['nmkelas'];
							$kdkonsen=$datav['kdkonsen'];
							$JUR=substr($KELAS,2,2);
							if($kdkonsen=="OF")
							{
							$JUR="61401";
							$KONSEN="MANAJEMEN ADMINISTRASI OBAT DAN FARMASI";
							$KODEX="33";
							}elseif($kdkonsen=="RS")
							{
							$JUR="61401";
							$KODEX="03";
							$KONSEN="MANAJEMEN ADMINISTRASI RUMAH SAKIT";
							}
							elseif($kdkonsen=="TU")
							{
							$JUR="61401";
							$KODEX="13";
							$KONSEN="MANAJEMEN ADMINISTRASI TRANSPORTASI UDARA";
							}
							$PRODI="MANAJEMEN ADMINISTRASI";
							$MASUK=substr($KELAS,5,2);
							$pecahkelas=explode("/",$KELAS); 
							$kelas1=strtoupper($pecahkelas[0]); // buat huruf besar semua 
							$kelas2=strtoupper($pecahkelas[1]);
						
							$TAHUN="20$MASUK";
							$ASALXX="20$MASUK";
							$ASALXX2="1";
							$CEKXX=substr($tahunajaran,0,4);
							$CEKXX2=substr($tahunajaran,4,1);
							$CEKXXX=$CEKXX+1;
							if($ASALXX2==$CEKXX2)
							{
							$P=(($CEKXX-$ASALXX)*2)+1;
							}else
							{
							$P=(($CEKXX-$ASALXX)*2)+2;
							}

							if($P==1)
							{
							$R="I";
							$V=1;
							}elseif($P==2)
							{
							$R="II";
							$V=2;
							}elseif($P==3)
							{
							$R="III";
							$V=3;
							}elseif($P==4)
							{
							$R="IV";
							$V=4;
							}elseif($P==5)
							{
							$R="V";
							$V=5;
							}elseif($P==6)
							{
							$R="VI";
							$V=6;
							}elseif($P==7)
							{
							$R="VII";
							$V=7;
							}elseif($P==8)
							{
							$R="VIII";
							$V=8;
							}elseif($P==9)
							{
							$R="IX";
							$V=9;
							}elseif($P==10)
							{
							$R="X";
							$V=10;
							}elseif($P==11)
							{
							$R="XI";
							$V=11;
							}elseif($P==12)
							{
							$R="XII";
							$V=12;
							}elseif($P==13)
							{
							$R="XIII";
							$V=13;
							}elseif($P==14)
							{
							$R="XIV";
							$V=14;
							}elseif($P==15)
							{
							$R="XV";
							$V=15;
							}else
							{
							$R="??";
							}
							
							
							$CEKXX=substr($tahunajaran,0,4);
							$CEKXX2=substr($tahunajaran,4,1);
							
							$CEKXXX=$CEKXX+1;
							$P=substr($tahunajaran,4,1);
							if(($P%2)==1)
							{
							$SS="GANJIL";
							$LALUX=($CEKXX-1);
							$SP="2";
							$LALU="$LALUX$SP";
							}else
							{
							$SS="GENAP";
							$LALUX=$CEKXX;
							$SP="1";
							$LALU="$LALUX$SP";
							}
							$hasil3 = mysql_query("select NLIPSTRAKM,SKSEMTRAKM from trakm where NIMHSTRAKM = '$id_mhs' and THSMSTRAKM='$LALU'");
							$data3 = mysql_fetch_array($hasil3);
							$adalalu = mysql_num_rows($hasil3);
								if($adalalu<=0)
												{
							$hasillalu = mysql_query("select THSMSTRAKM,NLIPSTRAKM,SKSEMTRAKM from trakm where NIMHSTRAKM = '$id_mhs' order by THSMSTRAKM DESC limit 0,1");
							$datalalu = mysql_fetch_array($hasillalu);
							$NLIPSTRAKM=$datalalu['NLIPSTRAKM'];
							$SKSEMTRAKM=$datalalu['SKSEMTRAKM'];
							$THSMSTRAKMlalu=$datalalu['THSMSTRAKM'];
												}else
												{
							$NLIPSTRAKM=$data3['NLIPSTRAKM'];
							$SKSEMTRAKM=$data3['SKSEMTRAKM'];
							
							}
							if($NLIPSTRAKM>=3.00)
							{
							$SKSMAX="24";
							}elseif($NLIPSTRAKM>=2.50)
							{
							$SKSMAX="22";
							}elseif($NLIPSTRAKM>=2.00)
							{
							$SKSMAX="20";
							}elseif($NLIPSTRAKM>1.50)
							{
							$SKSMAX="18";
							}
							else
							{
							$SKSMAX="15";
							}
					?>
							<table class="identitas" border="0" cellpadding="0" cellspacing="0" width="100%">
									<tbody><tr valign="top">
										<td width="11%">N A M A</td><td width="1%">:</td><td width="250"><strong><? print($data['NMMHSMSMHS']); ?></strong></td>
										<td width="16%">No. Mahasiswa</td><td width="1%">:</td><td width="200"><strong><? print("$nim"); ?></strong></td>

									</tr>
									<tr valign="top">
										<td>Prodi</td><td width="1%">:</td><td width="250"><strong><? print($PRODI); ?></strong></td>
										<td width="16%">Kelas / Semester</td><td width="1%">:</td><td width="200"><strong><? print("$KELAS / $R"); ?></strong></td>
									</tr>
									
									<tr valign="top">

																				<td>Konsentrasi</td><td width="1%">:</td><td width="250"><strong><? print($nmkonsen); ?></strong></td>
																				<td width="16%"></td><td width="1%"></td><td width="200"><strong></strong></td>
									</tr>
									
								</tbody></table><br>
<?
					  echo '<table width="100%" cellspacing="2" cellpadding="2" class="ewTable" border=1><tr >'
					.'<td   width="5%" align="center" rowspan=2><div class="identitas">No</div></td>'
					.'<td    width="10%" align="center" rowspan=2><div class="identitas">Kode</div></td>'
					.'<td   width="60%" align="center" rowspan=2><div class="identitas">Mata Kuliah</div></td>'
					.'<td   align="center" rowspan=2><div class="identitas">SKS</div></td>'
					.'<td  align="center" colspan=2><div class="identitas">Nilai</div></td>'
					.'<td   align="center" rowspan=2><div class="identitas">NAxSKS</div></td>'
					.'</tr>';
								  echo '<tr>'
					.'<td   align="center"><div class="identitas">Huruf</div></td>'
					.'<td   align="center"><div class="identitas">Bobot</div></td>'

					.'</tr>';
				// $totip2 = "SELECT distinct(KDKMKTRNLM) from trnlm  where NIMHSTRNLM='$id_mhs' and ((NLAKHTRNLM<>'') AND (NLAKHTRNLM<>'T') AND (NLAKHTRNLM<>'0'))  order by KDKMKTRNLM,NLAKHTRNLM ASC";

if($ipkaktif=="yes")
{
$totip2 = "SELECT distinct(KDKMKTRNLM) from trnlm  where NIMHSTRNLM='$id_mhs'  order by KDKMKTRNLM,NLAKHTRNLM ASC";
}else
{
$totip2 = "SELECT distinct(KDKMKTRNLM) from trnlm  where NIMHSTRNLM='$id_mhs' and THSMSTRNLM<>'$tahunajaran'  order by KDKMKTRNLM,NLAKHTRNLM ASC";
}
				  
								
											  $hasilip2 = mysql_query($totip2);
											  $no=0;
											  $nasks2=0;
											  $totnasksipk2=0;
											  $totsksipk2=0;
											  $ipk2=0;
											    while($dataip2 = mysql_fetch_array($hasilip2))
												{
													$warna = ($no % 2) ? "#d8f5b3" : "#f5fdec";
												
												$kode2= $dataip2["KDKMKTRNLM"];
												 $totip3 = "SELECT NLAKHTRNLM,BOBOTTRNLM,THSMSTRNLM,KDKMKTRNLM from trnlm  where NIMHSTRNLM='$id_mhs' and KDKMKTRNLM='$kode2' order by NLAKHTRNLM ASC LIMIT 0,1";
											  $hasilip3 = mysql_query($totip3);
											  $dataip3 = mysql_fetch_array($hasilip3);
													
													$nilai2= $dataip3["NLAKHTRNLM"];
													$bobot2= $dataip3["BOBOTTRNLM"];
													$THSMSTRNLM= $dataip3["THSMSTRNLM"];
													$kode3= $dataip3["KDKMKTRNLM"];
													
													 $totmk2= "SELECT m.SKSMKTBKMK,m.NAKMKTBKMK from tbkmk m where m.KDKMKTBKMK='$kode3' and m.THSMSTBKMK='$THSMSTRNLM' and m.KDPSTTBKMK='$JUR' and (kdkonsen='u' or kdkonsen='$kdkonsen')";
											  $hasilmk2 = mysql_query($totmk2);
											 
											 $datamk2 = mysql_fetch_array($hasilmk2);
											  $sks2= $datamk2["SKSMKTBKMK"];
											    $namamk= $datamk2["NAKMKTBKMK"];
											$nasks2=$sks2*$bobot2;
													$totnasksipk2=$totnasksipk2+$nasks2;
													$totsksipk2=$totsksipk2+$sks2;
													if($totsksipk2<=0)
													{
													$ipk2="0.00";
													}else
													{
													$ipk2=$totnasksipk2/$totsksipk2;
													$ipk2=number_format($ipk2,2); 
													}
												
												 echo '<tr>'
						.'<td class="identitas"  align="center">'.($no+1).'</td>'
						.'<td class="identitas"  align="center">'.$kode3.'</td>';
						
						if($nilai2=="" or $nilai2=="T" or $nilai2=="E" or $nilai2=="0")
						{
						 echo'<td align="left" class="identitas" >'.$namamk.'</td>';
						 }else
						 {
						  echo'<td align="left" class="identitas" >'.$namamk.'</td>';
						 }
						 
							 echo'<td class="identitas"  align="center">'.$sks2.'</td>'
								.'<td class="identitas"  align="center">'.$nilai2.'</td>'
									.'<td class="identitas"  align="center">'.number_format($bobot2,2).'</td>'
										.'<td class="identitas" align="center">'.number_format($nasks2,2).'</td>'
				
					  .'</tr>';	
					  $no++;
												}
				
				$warna = ($no % 2) ? "#d8f5b3" : "#f5fdec";
				  echo '<tr>'
						.'<td align="center" colspan=3  class="identitas">JUMLAH</td>'
						
							.'<td align="center"  class="identitas"><b>'.$totsksipk2.'</b></td>'
								.'<td  align="center"></td>'
									.'<td align="center"></td>'
										.'<td  align="center"  class="identitas"><b>'.number_format($totnasksipk2,2).'</b></td>'
				
					  .'</tr>';	  
				//jika data masih kosong
				if ($no < 1 ){
					 echo '<tr>'
							.'<td height="30" colspan="7">'
							.'<div align="center" style="border:1px solid #9ecf5a;padding-top:15px;padding-bottom:15px;">Anda belum pernah KRS</div>'
							.'</td>'
						 .'</tr>';
						 	echo '</table>'
					.'</form>';
				} else {
				
$ipsem=$ipk2;
if($ipsem>=3.51)
{
$predikat="Cumlaude";
}elseif($ipsem>=2.76)
{
$predikat="Sangat Memuaskan";
}elseif($ipsem>=2)
{
$predikat="Memuaskan";
}elseif($ipsem>0)
{
$predikat="Kurang Memuaskan";
}elseif($ipsem<=0)
{
$predikat="--";
}
$engDate=date("l F d, Y H:i:s A");
//echo "English Date : ". $engDate ."<p>";

switch (date("w")) {
case "0" : $hari="Minggu";break;
case "1" : $hari="Senin";break;
case "2" : $hari="Selasa";break;
case "3" : $hari="Rabu";break;
case "4" : $hari="Kamis";break;
case "5" : $hari="Jumat";break;
case "6" : $hari="Sabtu";break;
} switch (date("m")) {
case "1" : $bulan="Januari";break;
case "2" : $bulan="Februari";break;
case "3" : $bulan="Maret";break;
case "4" : $bulan="April";break;
case "5" : $bulan="Mei";break;
case "6" : $bulan="Juni";break;
case "7" : $bulan="Juli";break;
case "8" : $bulan="Agustus";break;
case "9" : $bulan="September";break;
case "10" : $bulan="Oktober";break;
case "11" : $bulan="November";break;
case "12" : $bulan="Desember";break;
}
$indDate="". date("d") ." $bulan". date(" Y");

				  echo '</table><br><table><tr>'
						.'<td align="left" colspan=7  class="identitas2">Indeks Prestasi Kumulatif (IPK ) : <b>'.$ipk2.'</b><br>
						Predikat : <b>'.$predikat.'</b>
						</td>'
					  .'</tr>';	
	echo '</table>';
	
	 echo '<br><br><table width="90%"><tr><td width="50%"></td>'
						.'<td align="center" class="identitas2">Yogyakarta, '.$indDate.'<br>AMA Yogyakarta
						<br><img src="source/ttd.jpg" style="height:70px;border-width:0px;"><br><br>
								<strong>Wahyudiyono, S.E., M.M.</strong><hr>
								Wakil Direktur I
						</td>'
					  .'</tr>';	
	echo '</table>'
					.'</form>';					  	  				
				}
					 
				
			
?>
</body>
</html>
