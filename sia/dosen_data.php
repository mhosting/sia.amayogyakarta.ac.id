<?
include_once("reference/config.php");
 ?>
<html><head>
<meta http-equiv="content-type" content="text/html; charset=ISO-8859-1">
<title></title>
<link href="include/style000.css" rel="stylesheet" type="text/css">

<link href="source/reset001.css" rel="stylesheet" type="text/css" /><link href="source/common01.css" rel="stylesheet" type="text/css" /><link href="source/form0001.css" rel="stylesheet" type="text/css" /><link href="source/standare.css" rel="stylesheet" type="text/css" /><link href="source/96000001.css" rel="stylesheet" type="text/css" /><link href="source/simple-m.css" rel="stylesheet" type="text/css" /><link href="source/block-lj.css" rel="stylesheet" type="text/css" /><link href="source/table001.css" rel="stylesheet" type="text/css" /><link href="source/calendas.css" rel="stylesheet" type="text/css" /><link href="source/wizard01.css" rel="stylesheet" type="text/css" />
    <!-- Favicon -->
 
</head>
<body><script language="javascript" type="text/javascript" runat="server">

function date_valid_msg(caption, inp) {
	var s = caption + " harus diisi dengan urutan tgl-bln-thn.\nContoh input yang benar: 21-12-2006";
	if (inp.length < 10) return s;	
	else {		
		var temp = inp.split("-");
		var test = new Date(temp[1] + '/' + temp[0] + '/' + temp[2]);
		if (test == "Invalid Date") return s;
		else return "";
	}
}

function semester_valid_msg(caption,inp) {
	var s = caption + " harus diisi angka sepanjang 5 digit.\n";
	s += "Contoh input yang benar:\n";
	s += "20061 => T.A. 2006 Semester Ganjil\n";
	s += "20062 => T.A. 2006 Semester Genap";
	if (isNaN(inp) == "NaN") return s;
	else if (inp.length < 5) return s;
	else if (inp.substr(4,1) != "1"  && inp.substr(4,1) != "2") return s;
	else return "";
}

function blnthn_valid_msg(caption,inp) {
	var s = caption + " harus diisi angka sepanjang 6 digit.\n";
	s += "Contoh input yang benar:\n";
	s += "012005 => Bulan Januari 2005\n";
	s += "092006 => Bulan September 2006";
	if (isNaN(inp) == "NaN") return s;
	else if (inp.length < 6) return s;
	else return "";
}

function show_popup(url, w, h)
{
	if (w != null) var width = w; else var width = 500;
	if (h != null) var height = h; else var height = 700;
	var left = (screen.width-width)/2;
	var top = (screen.height-height)/2;
	window.open(url,'','top=' + top + ', scrollbars, left=' + left + ',height=' + height + ',width=' + width);
}

function show_popup2(url, w, h, target)
{
	if (w != null) var width = w; else var width = 500;
	if (h != null) var height = h; else var height = 700;
	var left = (screen.width-width)/2;
	var top = (screen.height-height)/2;
	window.open(url,target,'top=' + top + ', scrollbars, left=' + left + ',height=' + height + ',width=' + width);
}

function num_larger(inp, a) 
{
	var num = parseInt(inp);
	if (isNaN(inp)) return false;
	else if (num <= a) return false;
	else return true;
}

function nilai_huruf(txtField) /* =============== add by aji -- 26/02/2007 */
{
  	var checkOK = "ABCDET+-";
  	var checkStr = txtField.value;
  	var allValid = true;
  	var decPoints = 0;
  	var allNum = "";

  	for (i = 0;  i < checkStr.length;  i++)
	{
    	ch = checkStr.charAt(i);
    	for (j = 0;  j < checkOK.length;  j++)
      		if (ch == checkOK.charAt(j))
        		break;
    		if (j == checkOK.length)
			{
      			allValid = false;
      			break;
    		}
  		}

  		if (!allValid)
		{
			txtField.value = "";
			alert('Diisi huruf Kapital A / B / C / D / E / T \ndan/atau diikuti tanda (+) atau (-). \n\nContoh : A+, A, A-');
    		return (false);
  		}
	return (true);
}

function nilai_desimal(txtField) /* =============== add by aji -- 26/02/2007 */
{
  	var checkOK = "0123456789-.,";
  	var checkStr = txtField.value;
  	var allValid = true;
  	var decPoints = 0;
  	var allNum = "";

  	for (i = 0;  i < checkStr.length;  i++)
	{
    	ch = checkStr.charAt(i);
    	for (j = 0;  j < checkOK.length;  j++)
      		if (ch == checkOK.charAt(j))
        		break;
    		if (j == checkOK.length)
			{
      			allValid = false;
      			break;
    		}
    		if (ch == ".")
			{
      			allNum += ".";
      			decPoints++;
    		}
    		else if (ch != ",")
      			allNum += ch;
  		}

  		if (!allValid)
		{
			txtField.value = "0.00";
			alert('Diisi angka 0~9 dengan menggunakan pemisah desimal <u>titik</u>. Contoh : 4.00 ; 3.50 ; 2.55');
    		return (false);
  		}
	return (true);
}

// aji : 23/08/2007
function konfirm(msg,url){
	if(confirm(msg)){
		if(url){window.location.href=url;}
	}else{return false;}
}

</script>
<form name="frm" method="post">
<script language="JavaScript">
function refresh_grid() {
	// var f = document.frm
	// var id = f.id.value
	// var sortby = f.sortby.value
	// var page = f.page.value
	// var scriptname = f.scriptname.value
	// window.location = '?exe=list&id=' + id + '&sortby=' + sortby + '&page=' + page
	document.frm.submit();
}
function go_back() {
	var f = document.frm
	var id = f.id.value
	var scriptname = f.scriptname.value
	window.location = '?id=' + id
}
</script>
<?
$id=$_GET['id'];
$opsi=$_POST['opsi'];
  ?>
	<table width="100%">	<tbody><tr class="trjudul" height="25">
			<td class="judulmenu" style="border-right: 0px none;">&nbsp;Daftar Dosen</td>
			<td class="judulmenu" style="border-right: 0px none; border-left: 0px none;" align="right">
				<input class="textbox" type="text" name="kunci" size="15" value="">
	<input  type="submit" value="Cari" title="Klik untuk mencari" name="cari">
	<input  type="submit" value="Refresh" title="Klik untuk menampilkan semua data Dosen" onclick="document.frmt.txtCari.value=''">&nbsp;</nobr>
		
		</td>
		</tr>
	</tbody></table>
	<?
   $kunci=$_POST['kunci'];
  if(isset($_POST['cari']))
   {
   if( $id<>'')
  {
  $$qall = "SELECT *,DATE_FORMAT(TGLHRMSDOS,'%d-%m-%Y') AS tgl_lahir FROM msdos where (NODOSMSDOS like '%$kunci%' or NMDOSMSDOS like '%$kunci%' or TPLHRMSDOS like '%$kunci%' or NIDNNMSDOS like '%$kunci%' )";
  }else
  {
   $qall = "SELECT *,DATE_FORMAT(TGLHRMSDOS,'%d-%m-%Y') AS tgl_lahir FROM msdos where (NODOSMSDOS like '%$kunci%' or NMDOSMSDOS like '%$kunci%' or TPLHRMSDOS like '%$kunci%' or NIDNNMSDOS like '%$kunci%' )";
   }
   $hasilall = mysql_query($qall);  
$jum=mysql_num_rows($hasilall);    
   }else
   {
     if( $id<>'')
  {
   $qall = "SELECT *,DATE_FORMAT(TGLHRMSDOS,'%d-%m-%Y') AS tgl_lahir FROM msdos";
   }else
   {
   $qall = "SELECT *,DATE_FORMAT(TGLHRMSDOS,'%d-%m-%Y') AS tgl_lahir FROM msdos";
   }
   }
   
		$hasilall = mysql_query($qall);   
		$noxcc=0;
				while($dataall = mysql_fetch_array($hasilall))
		{
		$noxcc++;
				
			$tgl_lahir=$dataall["tgl_lahir"];					 
			$jenjang=$dataall["KDJENMSDOS"];									  
					$nodos=$dataall["NODOSMSDOS"];	
								$nama=$dataall["NMDOSMSDOS"];	
					$gelar=$dataall["GELARMSDOS"];	
					$tempat=$dataall["TPLHRMSDOS"];	
					$prodi=$dataall["KDPSTMSDOS"];	
$NIDNNMSDOS=$dataall["NIDNNMSDOS"];	

if($jenjang=="E")
{
$jenjangnya="E-D3";
} 
}
?>
	<table class="table" rules="all" onsortcommand="SortData" datakeyfield="ID" id="ctl00_ContentPlaceHolder2_dgAsgnm" style="background-color: rgb(239, 239, 239); border-color: Silver; border-width: 1px; border-style: solid; width: 100%; border-collapse: collapse;" align="Left" border="1" cellpadding="2" cellspacing="0">
		<tbody><tr class="readonly">
			<td width="20">No</td>
			<td align="left" width="50"><a href="javascript:refresh_grid()" onclick="document.frm.sortby.value='NODOSMSDOS';document.frm.page.value='1'" title="Urutkan berdasar No.Dosen">No.Dosen</a></td>
			<td align="left" width="375"><a href="javascript:refresh_grid()" class="sort" onclick="document.frm.sortby.value='NMDOSMSDOS';document.frm.page.value='1'" title="Urutkan berdasar Nama Dosen &amp; Gelar">Nama Dosen &amp; Gelar</a></td>
				<td scope="col" nowrap="nowrap">&nbsp;&nbsp;Tempat Lahir</td>
			<td scope="col" nowrap="nowrap">&nbsp;&nbsp;Tgl Lahir</td>
		</tr>
		<?
		 $kunci=$_POST['kunci'];
  if(isset($_POST['cari']))
   {
   if( $id<>'')
  {
  $$qall = "SELECT *,DATE_FORMAT(TGLHRMSDOS,'%d-%m-%Y') AS tgl_lahir FROM msdos where (NODOSMSDOS like '%$kunci%' or NMDOSMSDOS like '%$kunci%' or TPLHRMSDOS like '%$kunci%' or NIDNNMSDOS like '%$kunci%' )";
  }else
  {
   $qall = "SELECT *,DATE_FORMAT(TGLHRMSDOS,'%d-%m-%Y') AS tgl_lahir FROM msdos where (NODOSMSDOS like '%$kunci%' or NMDOSMSDOS like '%$kunci%' or TPLHRMSDOS like '%$kunci%' or NIDNNMSDOS like '%$kunci%' )";
   }
   $hasilall = mysql_query($qall);  
$jum=mysql_num_rows($hasilall);    
   }else
   {
     if( $id<>'')
  {
   $qall = "SELECT *,DATE_FORMAT(TGLHRMSDOS,'%d-%m-%Y') AS tgl_lahir FROM msdos";
   }else
   {
   $qall = "SELECT *,DATE_FORMAT(TGLHRMSDOS,'%d-%m-%Y') AS tgl_lahir FROM msdos";
   }
   }
   
		$hasilall = mysql_query($qall);   
		$noxcc=0;
				while($dataall = mysql_fetch_array($hasilall))
		{
		$noxcc++;
				
			$tgl_lahir=$dataall["tgl_lahir"];					 
			$jenjang=$dataall["KDJENMSDOS"];									  
					$nodos=$dataall["NODOSMSDOS"];	
								$nama=$dataall["NMDOSMSDOS"];	
					$gelar=$dataall["GELARMSDOS"];	
					$tempat=$dataall["TPLHRMSDOS"];	
					$prodi=$dataall["KDPSTMSDOS"];	
$NIDNNMSDOS=$dataall["NIDNNMSDOS"];	

if($jenjang=="E")
{
$jenjangnya="E-D3";
} 
											  ?>
		
		<tr class="bg2">
			 <td align="center" ><? print("$noxcc");?></td>
                                              <td align="center" valign="center"><? print("$nodos");?></td>
											   <td><a class="link" href="javascript:window.opener.hook_dosen('<? print("$nodos");?>','<? print("$nama");?>');window.close();"><? print("$nama");?></a>&nbsp;&nbsp;</td>
											  
												   <td>&nbsp;&nbsp;<? print("$tempat");?></td>
												    <td>&nbsp;&nbsp;<? print("$tgl_lahir");?></td>   
		</tr>
		<?
						
							   }
							  
							   ?>
	</tbody></table>
	<table style="border: 1px solid rgb(0, 51, 153);" cellpadding="0" cellspacing="0" width="100%">
		<tbody><tr class="trjudul" height="25">
			<td style="border-left: 0px none; padding: 0px;" align="left">&nbsp;<? print("$jum");?> data</td>
			<td style="border-left: 0px none; padding: 0px;" align="right">

	</td>
		</tr>
	</tbody></table>
<input name="exe" type="hidden">
<input name="id" value="" type="hidden">
<input name="sortby" value="NMDOSMSDOS" type="hidden">
<input name="searchby" value="" type="hidden">
<input name="page" value="1" type="hidden">
<input name="scriptname" value="" type="hidden">
<script>document.frm.txtCari.focus()</script></form></body></html>