<?
session_start();
define(md5('ama') ."". base64_encode('perpus'),true);
if(!isset($_SESSION['adminid']) && !isset($_SESSION['adminnama']))
	{
		header("Location: login.php?route=info/infoperpus");
		die();
	}  else {
	include_once("reference/config.php");

	$just=$_SESSION["statusss"];

?>
<html>
<head>
<title>PERPUSTAKAAN AMA YOGYAKARTA</title>

<link rel="stylesheet" type="text/css" href="include/styleshe.css" />
<link rel="stylesheet" type="text/css" href="include/ui000000.css" />
<script type="text/javascript" src="include/jquery-1.js"></script>
<script type="text/javascript" src="include/ui000000.js"></script>
<script type="text/javascript" src="include/superfis.js"></script>
<script type="text/javascript" src="include/tab00000.js"></script>
<script src="include/jquery00.txt" type="text/javascript"></script>


<script type="text/javascript">
//-----------------------------------------
// Confirm Actions (delete, uninstall)
//-----------------------------------------
$(document).ready(function(){
	
    // Confirm Delete
    $('#form').submit(function(){
        if ($(this).attr('action').indexOf('delete',1) != -1) {
            if (!confirm ('Apakah anda yakin ingin melakukannya?')) {
                return false;
            }
        }
    });
    	
    // Confirm Uninstall
    $('a').click(function(){
        if ($(this).attr('href') != null && $(this).attr('href').indexOf('uninstall',1) != -1) {
            if (!confirm ('Apakah anda yakin ingin melakukannya?')) {
                return false;
            }
        }
    });
});
</script>
<script type="text/javascript">
$(document).ready(function(){
    $(".scrollbox").each(function(i) {
    	$(this).attr('id', 'scrollbox_' + i);
		sbox = '#' + $(this).attr('id');
    	$(this).after('<span><a onclick="$(\'' + sbox + ' :checkbox\').attr(\'checked\', \'checked\');"><u>Pilih semua</u></a> / <a onclick="$(\'' + sbox + ' :checkbox\').attr(\'checked\', \'\');"><u>Tidak pilih semua</u></a></span>');
	});
});
</script>

<script type="text/javascript">
$(document).ready(function() {
  $(".list tr:even").css("background-color", "#F4F4F8");
});
</script>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
</head>
<body>
<div id="container">
<div id="header">
  <div class="div1"><img src="include/logo0000.png" align="top" title="Pengaturan" onclick="location = '?route='" /></div>
    <div class="div2"><img src="include/user1_lo.png" alt="" style="position: relative; top: 6px;" />&nbsp;Anda Log in sebagai: <span><? print($just); ?></span></div>
  </div>
<div id="menu">
  <ul class="nav left" style="display: none;">
  
  <?
  if($just=="admin")
  {
  ?>
 
    <li id="beranda"><a href="?route=beranda" class="top">Beranda</a></li>
    <li id="anggota"><a class="top">Anggota</a>
      <ul>
	   <li><a href="?route=anggota/anggota">Mahasiswa</a></li>
	    <li><a href="?route=anggota/umum">Umum</a></li>
        <li><a href="?route=anggota/cetakmassal">Cetak Barcode Massal</a></li>
      </ul>
    </li>
    <li id="inventarisasi"><a class="top">Inventarisasi</a>
      <ul>
        <li><a href="?route=inventarisasi/kategori">Kategori</a></li>
        <li><a href="?route=inventarisasi/penerbit">Penerbit</a></li>
        <li><a href="?route=inventarisasi/buku/tambah">Tambah Buku</a></li>
        <li><a href="?route=inventarisasi/buku">Lihat Buku</a></li>
        <li><a href="?route=inventarisasi/inventaris">Inventaris</a></li>
		<li><a href="?route=inventarisasi/hilangrusak">Buku Hilang/Rusak</a></li>
		<li><a href="?route=inventarisasi/ta">Tugas Akhir</a></li>
      </ul>
    </li>
    <li id="transaksi"><a class="top">Transaksi</a>
      <ul>
        <li><a href="?route=transaksi/sirkulasi">Sirkulasi Anggota</a></li>
		<li><a href="?route=transaksi/kembalibuku">Pengembalian Buku</a></li>
		 <li><a href="?route=transaksi/bebaspustaka">Bebas Pustaka</a></li>
      </ul>
    </li>
    <li id="system"><a class="top">System</a>
      <ul>
        <li><a href="?route=system/operator">Operator</a></li>
        <li><a href="?route=system/konfigurasi">Konfigurasi</a></li>
		 <li><a href="?route=system/libur">Hari Libur</a></li>
		  <li><a href="?route=system/info">Informasi</a></li>
      </ul>
    </li>
    <li id="laporan"><a class="top">Laporan</a>
      <ul>
	   <li><a href="?route=laporan/lapanggota">Anggota</a></li>
     <li><a onclick="window.open('laporan-buku.php');">Buku</a></li>
	 <li><a onclick="window.open('laporan-hilangrusak.php');">Buku Hilang/Rusak</a></li>
		<li><a onclick="window.open('laporan-denda.php');">Denda</a></li>
		 <li><a onclick="window.open('laporan-pinjam.php');">Peminjaman</a></li>
		 <li><a onclick="window.open('laporan-kembali.php');">Pengembalian</a></li>
		  <li><a onclick="window.open('laporan-buku-belum.php');">Buku Belum Kembali</a></li>
		    <li><a onclick="window.open('laporan-status.php');">Anggota Pinjam</a></li>
       
      </ul>
    </li>
	  <li id="help"><a class="top">Help</a>
      <ul>
       <li><a onclick="window.open('dokumentasi.pdf');">Dokumentasi</a></li>
        
      </ul>
    </li>
	<?
	}if($just=="pelayanan")
  {
  ?>
  <li id="beranda"><a href="?route=beranda" class="top">Beranda</a></li>
    <li id="anggota"><a class="top">Anggota</a>
      <ul>
	   <li><a href="?route=anggota/anggota">Mahasiswa</a></li>
	    <li><a href="?route=anggota/umum">Umum</a></li>
      
      </ul>
    </li>
	<li id="inventarisasi"><a class="top">Inventarisasi</a>
      <ul>
        <li><a href="?route=inventarisasi/buku">Lihat Buku</a></li>
        <li><a href="?route=inventarisasi/inventaris">Inventaris</a></li>
		<li><a href="?route=inventarisasi/hilangrusak">Buku Hilang/Rusak</a></li>
      </ul>
    </li>
   <li id="sirkulasi"><a href="?route=transaksi/sirkulasi" class="top">Sirkulasi</a></li>
   <li id="sirkulasi"><a href="?route=transaksi/kembalibuku" class="top">Pengembalian</a></li>
   <li id="sirkulasi"><a href="?route=transaksi/bebaspustaka" class="top">Bebas Pustaka</a></li>
   
    <li id="laporan"><a class="top">Laporan</a>
      <ul>
	   <li><a href="?route=laporan/lapanggota">Anggota</a></li>
     <li><a onclick="window.open('laporan-buku.php');">Buku</a></li>
	 <li><a onclick="window.open('laporan-hilangrusak.php');">Buku Hilang/Rusak</a></li>
		<li><a onclick="window.open('laporan-denda.php');">Denda</a></li>
		 <li><a onclick="window.open('laporan-pinjam.php');">Peminjaman</a></li>
		 <li><a onclick="window.open('laporan-kembali.php');">Pengembalian</a></li>
		  <li><a onclick="window.open('laporan-buku-belum.php');">Buku Belum Kembali</a></li>
		    <li><a onclick="window.open('laporan-status.php');">Anggota Pinjam</a></li>
       
      </ul>
    </li>
	 
    
  <?
  }if($just=="pengolahan")
  {
  ?>
 
    <li id="beranda"><a href="?route=beranda" class="top">Beranda</a></li>
    <li id="anggota"><a class="top">Anggota</a>
      <ul>
	   <li><a href="?route=anggota/anggota">Mahasiswa</a></li>
	    <li><a href="?route=anggota/umum">Umum</a></li>
        <li><a href="?route=anggota/cetakmassal">Cetak Barcode Massal</a></li>
      </ul>
    </li>
    <li id="inventarisasi"><a class="top">Inventarisasi</a>
      <ul>
        <li><a href="?route=inventarisasi/kategori">Kategori</a></li>
        <li><a href="?route=inventarisasi/penerbit">Penerbit</a></li>
        <li><a href="?route=inventarisasi/buku/tambah">Tambah Buku</a></li>
        <li><a href="?route=inventarisasi/buku">Lihat Buku</a></li>
        <li><a href="?route=inventarisasi/inventaris">Inventaris</a></li>
		<li><a href="?route=inventarisasi/hilangrusak">Buku Hilang/Rusak</a></li>
      </ul>
    </li>
    <li id="transaksi"><a class="top">Transaksi</a>
      <ul>
        <li><a href="?route=transaksi/sirkulasi">Sirkulasi Anggota</a></li>
		<li><a href="?route=transaksi/kembalibuku">Pengembalian Buku</a></li>
		 <li><a href="?route=transaksi/bebaspustaka">Bebas Pustaka</a></li>
      </ul>
    </li>
    <li id="laporan"><a class="top">Laporan</a>
      <ul>
	   <li><a href="?route=laporan/lapanggota">Anggota</a></li>
     <li><a onclick="window.open('laporan-buku.php');">Buku</a></li>
	 <li><a onclick="window.open('laporan-hilangrusak.php');">Buku Hilang/Rusak</a></li>
		<li><a onclick="window.open('laporan-denda.php');">Denda</a></li>
		 <li><a onclick="window.open('laporan-pinjam.php');">Peminjaman</a></li>
		 <li><a onclick="window.open('laporan-kembali.php');">Pengembalian</a></li>
		  <li><a onclick="window.open('laporan-buku-belum.php');">Buku Belum Kembali</a></li>
		    <li><a onclick="window.open('laporan-status.php');">Anggota Pinjam</a></li>
       
      </ul>
    </li>
	  <li id="help"><a class="top">Help</a>
      <ul>
       <li><a onclick="window.open('dokumentasi.pdf');">Dokumentasi</a></li>
        
      </ul>
    </li>
	<?
	}if($just=="pimpinan")
  {
  ?>
 
    <li id="beranda"><a href="?route=beranda" class="top">Beranda</a></li>
	 <li id="beranda"><a href="?route=inventarisasi/buku" class="top">Lihat Buku</a></li>
    <li id="laporan"><a class="top">Laporan</a>
      <ul>
	   <li><a href="?route=laporan/lapanggota">Anggota</a></li>
     <li><a onclick="window.open('laporan-buku.php');">Buku</a></li>
	 <li><a onclick="window.open('laporan-hilangrusak.php');">Buku Hilang/Rusak</a></li>
		<li><a onclick="window.open('laporan-denda.php');">Denda</a></li>
		 <li><a onclick="window.open('laporan-pinjam.php');">Peminjaman</a></li>
		 <li><a onclick="window.open('laporan-kembali.php');">Pengembalian</a></li>
		  <li><a onclick="window.open('laporan-buku-belum.php');">Buku Belum Kembali</a></li>
		    <li><a onclick="window.open('laporan-status.php');">Anggota Pinjam</a></li>
       
      </ul>
    </li>
	  <li id="help"><a class="top">Help</a>
      <ul>
       <li><a onclick="window.open('dokumentasi.pdf');">Dokumentasi</a></li>
        
      </ul>
    </li>
	<?
	}
	?>
	
	
  </ul>
  <ul class="nav right">
    <li id="store">
	<a onclick="window.open('?');" class="top"> <?
					  include("tgl.php");
					  ?> </a>
      <ul>
              </ul>
    </li>
	

    <li id="store"><a class="top" href="logout.php">Keluar</a></li>
  </ul>
  
  
  
  <script type="text/javascript"><!--
$(document).ready(function() {
	$('.nav').superfish({
		hoverClass	 : 'sfHover',
		pathClass	 : 'overideThisToUse',
		delay		 : 0,
		animation	 : {height: 'show'},
		speed		 : 'normal',
		autoArrows   : false,
		dropShadows  : false, 
		disableHI	 : false, /* set to true to disable hoverIntent detection */
		onInit		 : function(){},
		onBeforeShow : function(){},
		onShow		 : function(){},
		onHide		 : function(){}
	});
	
	$('.nav').css('display', 'block');
});
//--></script>
  <script type="text/javascript"><!-- 
function getURLVar(urlVarName) {
	var urlHalves = String(document.location).toLowerCase().split('?');
	var urlVarValue = '';
	
	if (urlHalves[1]) {
		var urlVars = urlHalves[1].split('&');

		for (var i = 0; i <= (urlVars.length); i++) {
			if (urlVars[i]) {
				var urlVarPair = urlVars[i].split('=');
				
				if (urlVarPair[0] && urlVarPair[0] == urlVarName.toLowerCase()) {
					urlVarValue = urlVarPair[1];
				}
			}
		}
	}
	
	return urlVarValue;
} 

$(document).ready(function() {
	route = getURLVar('route');
	
	if (!route) {
		$('#dashboard').addClass('selected');
	} else {
		part = route.split('/');
		
		url = part[0];
		
		if (part[1]) {
			url += '/' + part[1];
		}
		
		$('a[href*=\'' + url + '\']').parents('li[id]').addClass('selected');
	}
});
//--></script>
</div>


<div id="content">

  <?
	  $do= trim($_GET['route']);
$katabaru=explode("/",$do); 
$groupmenu=strtoupper($katabaru[0]); // buat huruf besar semua 
$route=strtoupper($katabaru[1]);
$opsi=strtoupper($katabaru[2]);
									if (empty($route))
										 {
											include "utama.php";
										 }
									else
									{
										switch ($route)
											{
											case "$route":
											if(file_exists("".$route.".php"))
											{
												include "".$route.".php";
												}else
												{
												include "utama.php";
												}
												break;
											default:
											if(file_exists("".$route.".php"))
											{
											include "".$route.".php";
											}else
											{
												include "utama.php";
												}
												break;
											}
									}
								?>

</div>





<div id="footer">
  Powered By <a href="">AMA YOGYAKARTA</a> &copy; 2011-2012 All Rights Reserved
</div>
</body></html>
   <?
}
mysql_close();
?>
