<?

?>
<script type="text/javascript">
function popup(url, w, h) {
	var kiri = (screen.width - w) / 2;
	var atas = (screen.height - h) / 2;
	p = 'height='+h+',width='+w+',top='+atas+',left='+kiri+'';
	win = window.open(url, '', p);
}

</script>
<script type="text/javascript">
function createRequestObject() {
	var ro;
	try {
		// Browser selain IE
		ro = new XMLHttpRequest();
	} catch (e) {
		// Internet Explorer
		try {
			ro = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try {
				ro = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e) {
				// Something went wrong
				alert("Browser does not support HTTP Request");
			}
		}
	}
	return ro;
}

var http = createRequestObject();

function sendRequest(action) {
	http.open('get', action);
	http.onreadystatechange = handleResponse;
	http.send(null);
}

function handleResponse() {
	if (http.readyState == 4) {
		var response = http.responseText;
		var update = new Array();

		if (response.indexOf('|' != -1)) {
			update = response.split('|');
			document.getElementById(update[0]).innerHTML = update[1];
		}
	}
}

function validasiBuku() {
	var pesan="";
	if(document.getElementById('no_klas').value=='') pesan+="- No. Klasifikasi belum diisi\n";
	if(document.getElementById('judul').value=='') pesan+="- Judul belum diisi\n";
	if(document.getElementById('penulis').value=='') pesan+="- Penulis belum diisi\n";
	if(document.getElementById('penerbit').value=='') pesan+="- Penerbit belum diisi\n";
	if(document.getElementById('thn_terbit').value=='') pesan+="- Tahun Terbit belum diisi\n";
	if(document.getElementById('isbn').value=='') pesan+="- ISBN belum diisi\n";
	if(document.getElementById('jml_hal').value=='') pesan+="- Jumlah Halaman belum diisi\n";
	if(document.getElementById('ukuran').value=='') pesan+="- Ukuran Fisik belum diisi\n";
	if(pesan!="") {
		var judul="Pengisian Form Belum Lengkap :\n";
		alert(judul+pesan);
		return false;
	}
}
function inputPenerbit() {
	document.getElementById('tambahpenerbit').style.display='inline';
	document.getElementById('namapenerbit').focus();
	document.getElementById('namapenerbit').value = '';
	document.getElementById('kotapenerbit').value = '';
}
function closeInputPenerbit() {
	document.getElementById('tambahpenerbit').style.display='none';
	
}
function updatePenerbit() {
	var np = document.getElementById('namapenerbit').value;
	var kp = document.getElementById('kotapenerbit').value;
	var pesan='';
	if(np=='') pesan+='- Nama Penerbit belum diisi\n';
	if(kp=='') pesan+='- Kota Penerbit belum diisi\n';
	if(pesan!="") {
		alert(pesan);
	} else {
		sendRequest('addbuku.php?action=penerbit&nama='+np+'&kota='+kp);
		closeInputPenerbit();
	}
}

</script>
<?
$limit= 40;
$start=$_GET['start'];
$page_name="?route=katalog/katalog";
?>

<div class="breadcrumb">
    <a href="">Home</a>
     :: <a href="">Katalog Buku</a>
  </div>
<div class="box">
  <div class="left"></div>
  <div class="right"></div>
  <div class="heading">
    <h1 style="background-image: url('include/buku.png');">Katalog Buku</h1>
    <div class="buttons"></div>
  </div>
  <div class="content">
  <?
 $order = addslashes(trim($_GET['order']));
	if($order=='DESC')
	{
	$flag="ASC";
	}else
	{
	$flag="DESC";
	}
  ?>
    <form action="" method="post" enctype="multipart/form-data" id="form">
      <table class="list">
        <thead>
          <tr>
            <td width="1" style="text-align: center;">NO</td>
            <td class="left">              <a href="?route=katalog/katalog&sort=b.no_klas&order=<? print($flag); ?>" class="asc">No. Klasifikasi</a>
              </td>
            <td class="left">              <a href="?route=katalog/katalog&sort=b.judul&order=<? print($flag); ?>">Judul Buku</a>
              </td>
            <td class="left">              <a href="?route=katalog/katalog&sort=b.penulis&order=<? print($flag); ?>">Penulis</a>
              </td>              
            <td class="left">              <a href="?route=katalog/katalog&sort=p.nama_penerbit&order=<? print($flag); ?>">Penerbit</a>
              </td>
            <td class="left">              <a href="?route=katalog/katalog&urut=k.nama_kategori&order=<? print($flag); ?>">Kategori</a>
              </td>              
            <td class="left">             Jumlah Buku
              </td>
            <td class="right">Aksi</td>
          </tr>
        </thead>
        <tbody>
          <tr class="filter">
            <td></td>
            <td><input type="text" name="filter_kode" value="" /></td>
            <td><input type="text" name="filter_judul" value="" /></td>
			<td><input type="text" name="filter_penulis" value="" /></td>
            <td><input type="text" name="filter_penerbit" value="" /></td>
			<td><input type="text" name="filter_kategori" value="" /></td>
			<td></td>
            <td align="right"><a onclick="filter();" class="button"><span>Filter</span></a></td>
          </tr>
		  <?
if(!isset($start)) {
$start = 0;
}

$eu = ($start - 0);

$ini = $eu + $limit;
$back = $eu - $limit;
$next = $eu + $limit;
		  //$qall = "SELECT s.no_buku,m.NIMHSMSMHS,m.NMMHSMSMHS,k.nmkelas,s.status,ms.NMPSTMSPST,m.KDJENMSMHS as jenjang,m.TPLHRMSMHS as tempatLahir,DAY(s.tanggal) as tanggalLahir,MONTH(s.tanggal) as bulanLahir,YEAR(s.tanggal) as tahunLahir FROM msmhs m,kelasparalel_mhs k,mspst ms,siperpus_buku s where s.noklas=m.NIMHSMSMHS and ms.KDPSTMSPST=m.KDPSTMSMHS and k.noklashs=m.NIMHSMSMHS order by s.no_barcode DESC limit 0,20";
		  
		 if($_GET['sort']<>'')
	{
	
	
	 $sort = addslashes(trim($_GET['sort']));
	$qall = "SELECT * from siperpus_buku b,siperpus_penerbit p,siperpus_kategori k where b.kd_penerbit=p.kd_penerbit and b.kd_kategori=k.kd_kategori order by $sort $flag  limit $eu, $limit";
	$qall2 = "SELECT * from siperpus_buku b,siperpus_penerbit p,siperpus_kategori k where b.kd_penerbit=p.kd_penerbit and b.kd_kategori=k.kd_kategori order by $sort $flag";
	
	} 
	 elseif($_GET['urut']<>'')
	{
	
	
	 $urut = addslashes(trim($_GET['urut']));
	$qall = "SELECT * from siperpus_buku b,siperpus_penerbit p,siperpus_kategori k where b.kd_penerbit=p.kd_penerbit and b.kd_kategori=k.kd_kategori order by $urut $flag  limit $eu, $limit";
	$qall2 = "SELECT * from siperpus_buku b,siperpus_penerbit p,siperpus_kategori k where b.kd_penerbit=p.kd_penerbit and b.kd_kategori=k.kd_kategori order by $urut $flag";
		
	} 
		  elseif($opsi=="FILTER")
	{
	$f_kode = addslashes(trim($_GET['f_kode']));
	$f_judul = addslashes(trim($_GET['f_judul']));
	$f_penerbit= addslashes(trim($_GET['f_penerbit']));
	$f_penulis = addslashes(trim($_GET['f_penulis']));
	$f_kategori = addslashes(trim($_GET['f_kategori']));

	
	$qall = "SELECT * from siperpus_buku b,siperpus_penerbit p,siperpus_kategori k where b.kd_penerbit=p.kd_penerbit and b.kd_kategori=k.kd_kategori and (b.no_klas like '%$f_kode%' AND b.penulis like '%$f_penulis%' AND p.nama_penerbit like '%$f_penerbit%' AND k.nama_kategori like '%$f_kategori%' and b.judul like '%$f_judul%') order by b.tanggal DESC  limit $eu, $limit";
	$qall2 = "SELECT * from siperpus_buku b,siperpus_penerbit p,siperpus_kategori k where b.kd_penerbit=p.kd_penerbit and b.kd_kategori=k.kd_kategori and (b.no_klas like '%$f_kode%' AND b.penulis like '%$f_penulis%' AND p.nama_penerbit like '%$f_penerbit%' AND k.nama_kategori like '%$f_kategori%' and b.judul like '%$f_judul%') order by b.tanggal DESC ";
		
	}else{
		  $qall = "SELECT * from siperpus_buku b,siperpus_penerbit p,siperpus_kategori k where b.kd_penerbit=p.kd_penerbit and b.kd_kategori=k.kd_kategori order by b.tanggal DESC limit $eu, $limit";
		  $qall2 = "SELECT * from siperpus_buku b,siperpus_penerbit p,siperpus_kategori k where b.kd_penerbit=p.kd_penerbit and b.kd_kategori=k.kd_kategori order by b.tanggal DESC";
		  	
		  }
		  $hasilall2 = mysql_query($qall2); 
		  $nume=mysql_num_rows($hasilall2);
		$hasilall = mysql_query($qall);   
		$no=0;
				while($dataall = mysql_fetch_array($hasilall))
		{
		$no++;
		$hal=$eu+$no;
			$kode= $dataall["no_klas"];
			$judul= $dataall["judul"];
			$penulis= $dataall["penulis"];

			$penerbit=$dataall['nama_penerbit'];
			$kategori=$dataall['nama_kategori'];
			$edit = "SELECT * FROM siperpus_inventaris where no_klas='$kode'";
		$editan = mysql_query($edit);
		 $jumpakai=mysql_num_rows($editan);
		  ?>
                              <tr>
            <td style="text-align: center;"> <? echo"$hal" ?>.            
              </td>
            <td class="left"><a href="javascript:;" onclick="popup('detail_buku.php?id=<? echo"$kode" ?>','550','380')" title="Lihat Detail Buku"><? echo"$kode" ?></a></td>
            <td class="left"><? echo"$judul" ?></td>
            <td class="left"><? echo"$penulis" ?></td>
            <td class="left"><? echo"$penerbit" ?></td>
            <td class="left"><? echo"$kategori" ?></td>
            <td class="left"><? echo"$jumpakai" ?></td>
			<td class="right">
              </td>
          </tr>
            <?
			}
			?>			
				   
				   
				   
				   
                            </tbody>
      </table>
    </form>
    <div class="pagination"><div class="results"><?
echo "";
print "";
if($back >=0) {
print "<a href='$page_name&start=$back' title='Prev'>PREV</a>";
}
echo "";
$i=0;
$l=1;
for($i=0;$i < $nume;$i=$i+$limit){
if($i <> $eu){
echo " <a href='$page_name&start=$i' title='$l' class='link'>$l</a> ";
}
else { echo "<font face='Verdana' size='2' color=red><b>$l</b></font>";}
$l=$l+1;
}


echo "";
if($ini < $nume) {
print "<a href='$page_name&start=$next' title='Next'>NEXT</a>";}
echo "";
?></div></div>
  </div>
</div>
<script type="text/javascript"><!--
function filter() {
	url = '?route=katalog/katalog/filter';
	
	var filter_kode = $('input[name=\'filter_kode\']').attr('value');
	
	if (filter_kode) {
		url += '&f_kode=' + encodeURIComponent(filter_kode);
	}
	var filter_judul = $('input[name=\'filter_judul\']').attr('value');
	
	if (filter_judul) {
		url += '&f_judul=' + encodeURIComponent(filter_judul);
	}
	var filter_penulis = $('input[name=\'filter_penulis\']').attr('value');
	
	if (filter_penulis) {
		url += '&f_penulis=' + encodeURIComponent(filter_penulis);
	}
	var filter_penerbit = $('input[name=\'filter_penerbit\']').attr('value');
	
	if (filter_penerbit) {
		url += '&f_penerbit=' + encodeURIComponent(filter_penerbit);
	}
	var filter_kategori = $('input[name=\'filter_kategori\']').attr('value');
	
	if (filter_kategori) {
		url += '&f_kategori=' + encodeURIComponent(filter_kategori);
	}
	
	
	
	location = url;
}
//--></script>
<script type="text/javascript" src="include/ui000001.js"></script>
<script type="text/javascript"><!--
$(document).ready(function() {
	$('#date').datepicker({dateFormat: 'yy-mm-dd'});
});
//--></script>
<script type="text/javascript"><!--
$.tabs('#tabs a'); 
$.tabs('#languages a'); 
//--></script>
</div>
<?


?>
